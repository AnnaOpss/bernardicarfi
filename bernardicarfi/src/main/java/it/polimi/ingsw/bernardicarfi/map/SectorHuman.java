package it.polimi.ingsw.bernardicarfi.map;

import it.polimi.ingsw.bernardicarfi.players.Player;

import java.io.IOException;

/**
 * @author Salvatore Carfì, Anna Bernardi
 * 
 */

public class SectorHuman extends Sector {

	public SectorHuman(int q, int r, int s) {
		super(q, r, s);
	}
	
	@Override
	public void walkIn(Player p) throws IOException {
		super.walkIn(p);
	}
}
