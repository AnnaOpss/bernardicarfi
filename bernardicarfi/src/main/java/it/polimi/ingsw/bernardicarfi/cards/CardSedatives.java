package it.polimi.ingsw.bernardicarfi.cards;

import it.polimi.ingsw.bernardicarfi.players.Player;

/**
 * @author Anna Bernardi, Salvatore Carfì
 *
 */

public class CardSedatives extends Card {
	
	/**
	 * Set a variable on a "player" which makes him unable to draw cards for this turn.
	 */
	@Override
	public void activateCard(Player p) {
		p.activateSedatives();
	}
	
	@Override
	public String name() {
		return "Sedativo";
	}
	
}
