package it.polimi.ingsw.bernardicarfi.map;

import java.io.IOException;

import it.polimi.ingsw.bernardicarfi.cards.Card;
import it.polimi.ingsw.bernardicarfi.decks.DeckSector;
import it.polimi.ingsw.bernardicarfi.players.Player;

/**
 * @author Salvatore Carfì
 * 
 */

public class SectorDangerous extends Sector {

	public SectorDangerous(int q, int r, int s) {
		super(q, r, s);
	}
	
	/**
	 * Notify to the player he walked into a dangerous sector, let him draw a sector card, activating it.
	 */
	@Override
	public void walkIn(Player p) throws IOException {
		super.walkIn(p);
		if (p.ableToDraw()) {
			Card c = DeckSector.takeCard();
			p.getNotifier().write("Sei entrato in un settore pericoloso e hai pescato una carta: " + c.name());
			c.activateCard(p); 
		}
	}
}
