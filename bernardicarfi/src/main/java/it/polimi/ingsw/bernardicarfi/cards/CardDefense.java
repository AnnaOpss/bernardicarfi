package it.polimi.ingsw.bernardicarfi.cards;

import it.polimi.ingsw.bernardicarfi.players.Player;

/**
 * @author Carfì Salvatore
 *
 */

public class CardDefense extends Card {
	
	/**
	 * Do nothing since the card defense cannot be used.
	 */
	@Override
	public void activateCard(Player p) {
		return;
	}
	
	@Override
	public String name() {
		return "Difesa";
	}
	
}
