package it.polimi.ingsw.bernardicarfi.cards;

import java.io.IOException;

import it.polimi.ingsw.bernardicarfi.decks.DeckItem;
import it.polimi.ingsw.bernardicarfi.players.Player;

/**
 * @author Carfì Salvatore
 *
 */
public class CardNoiseHereOBJ extends CardNoiseHere{

	/**
	 * Activates "CardNoiseHere" to make noise in this sector, also draw an items card.
	 */
	@Override
	public void activateCard(Player p) throws IOException {
		p.getNotifier().write("La carta settore che hai pescato ti fa pescare una carta oggetto");
		p.addCard(DeckItem.takeCard());
		super.activateCard(p);
	}	
}
