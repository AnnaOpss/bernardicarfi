package it.polimi.ingsw.bernardicarfi.cards;

import java.io.IOException;

import it.polimi.ingsw.bernardicarfi.Match;
import it.polimi.ingsw.bernardicarfi.map.Sector;
import it.polimi.ingsw.bernardicarfi.players.Player;


/**
 * @author Carfì Salvatore
 *
 */

public class CardNoiseHere extends Card {
	

/**
 * Notify to all players that this player made noise in a sector. The sector is the one he's in.
 */
	@Override
	public void activateCard(Player p) throws IOException {
		Sector currPos = p.getPosition(); 
		Match.notifyAll(p.getName() + ": RUMORE IN " + currPos.sectorToName());
	}
	
	@Override
	public String name() {
		return "Rumore nel tuo settore";
	}
}
