package it.polimi.ingsw.bernardicarfi.server.rmi;

import it.polimi.ingsw.bernardicarfi.Match;
import it.polimi.ingsw.bernardicarfi.server.Server;

import java.io.IOException;
import java.rmi.RemoteException;

/**
 * @author Salvatore Carfì, Anna Bernardi
 * 
 */

public class RMIHandler implements RemoteRMIHandler {
	private Server server;
	
	public RMIHandler (Server server) {
		this.server = server;
	}
	
	@Override
	public void startMatchIfReady() throws IOException {
		server.startMatchIfReady();
	}
	
	@Override
	public void addPlayer() throws IOException {
		server.addPlayer();
	}

	@Override
	public int getPlayersNum() throws RemoteException {
		return server.getPlayersNum();
	}
	
	@Override
	public int getIndex() throws RemoteException {
		return server.getIndex();
	}
	
	@Override
	public Match getMatch(int matchNum) throws RemoteException {
		return server.getMatch(matchNum);
	}
}
