package it.polimi.ingsw.bernardicarfi.decks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.bernardicarfi.Match;
import it.polimi.ingsw.bernardicarfi.cards.Card;
import it.polimi.ingsw.bernardicarfi.cards.CardAdrenaline;
import it.polimi.ingsw.bernardicarfi.cards.CardAttack;
import it.polimi.ingsw.bernardicarfi.cards.CardDefense;
import it.polimi.ingsw.bernardicarfi.cards.CardLight;
import it.polimi.ingsw.bernardicarfi.cards.CardSedatives;
import it.polimi.ingsw.bernardicarfi.cards.CardTeleport;

/**
 * @author Bernardi Anna
 *
 */

public class DeckItem  extends Deck {

	private static final int NUM_ITEM_CARDS = 12;

	private static List<Card> cards = new ArrayList<Card>(NUM_ITEM_CARDS);
	private static List<Card> refuses = new ArrayList<Card>(NUM_ITEM_CARDS);
	
	public DeckItem() {
		DeckItem.setDeck();
	}

	/**
	 * This method define a list of all item cards.
	 */
	public static void setDeck() {
		
		for (int i = 0; i < 2; i++) {
			cards.add(new CardAttack());
			cards.add(new CardAdrenaline());
			cards.add(new CardLight());
			cards.add(new CardTeleport());
			cards.add(new CardSedatives());
		}
		cards.add(new CardSedatives());
		cards.add(new CardDefense());
		
		cards = shuffle(cards);
	}
	
	/**
	 * @return a card. If the main deck is empty,
	 * it shuffle the refuse deck and put them in the main deck.
	 */
	public static Card takeCard() throws IOException {
		if (cards.isEmpty()) {
			cards = shuffle(refuses);
			refuses.clear();
		} 
		if (cards.isEmpty() && refuses.isEmpty()) {
			Match.notifyAll("No Items Cards left");
			return null;
		}
		Card result = cards.get(0);
		cards.remove(0);
		return result;
	}
	
	/**
	 * When a card is used, it has to be added in the refuse deck.
	 */
	public static void refuse (Card card) {		
		refuses.add(card);
	}
	
	public static int getNumCards() {
		return NUM_ITEM_CARDS;
	}
	
	public static List<Card> getDeck() {
		return cards;
	}
	
	public static List<Card> getRefuses() {
		return refuses;
	}
}