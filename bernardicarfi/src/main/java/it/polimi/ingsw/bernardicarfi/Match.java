package it.polimi.ingsw.bernardicarfi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.polimi.ingsw.bernardicarfi.decks.DeckCharacter;
import it.polimi.ingsw.bernardicarfi.decks.DeckEscapeHatch;
import it.polimi.ingsw.bernardicarfi.decks.DeckItem;
import it.polimi.ingsw.bernardicarfi.decks.DeckSector;
import it.polimi.ingsw.bernardicarfi.players.Alien;
import it.polimi.ingsw.bernardicarfi.players.Human;
import it.polimi.ingsw.bernardicarfi.players.Player;
import it.polimi.ingsw.bernardicarfi.server.RemoteNotifier;
import it.polimi.ingsw.bernardicarfi.map.GameMap;

/**
 * @author Bernardi Anna, Carfi Salvatore
 *
 */

public class Match {
	private static final int MAX_TURN = 39;
	private DeckCharacter dCharacter;
	@SuppressWarnings(value = { "unused" })
	private DeckItem dItem;
	@SuppressWarnings(value = { "unused" })
	private DeckEscapeHatch dHatch;
	@SuppressWarnings(value = { "unused" })
	private DeckSector dSector;
	@SuppressWarnings(value = { "unused" })
	private GameMap grid;
	
	private int currentTurn;
	private static List<Human> humans;
	private static List<Alien> aliens;
	private int humanIndex;
	private int alienIndex;
	private int humanNum;
	private int alienNum;
	private int currentPlayer;
	private static Map<Integer, Boolean> playersRole;
	private static Map<Integer, String> winners;
	private static List<Integer> deadPlayers;
	
	public Match(int playersNum) {
		dCharacter = new DeckCharacter(playersNum);
		dItem = new DeckItem();
		dHatch = new DeckEscapeHatch();
		dSector = new DeckSector();
		currentTurn = 0;
		humanIndex = 0;
		alienIndex = 0;	
		humanNum = 0;
		alienNum = 0;
		currentPlayer = 0;
		humans = new ArrayList<Human>();
		aliens = new ArrayList<Alien>();
		playersRole = new HashMap<Integer, Boolean>();
		winners = new HashMap<Integer, String>();
		deadPlayers = new ArrayList<Integer>();
		grid = GameMap.getInstance();
	}
	
	
	/**
	 * Draw a card from the character's deck. Assign the role on the "playersRole" (true alien, false human)
	 * also add the character to his respective list (alien/human). Then announces every player his role.
	 * @param keyNotifier (playerID)
	 * @param notifier
	 * @throws IOException
	 */
	public void addPlayer(int keyNotifier, RemoteNotifier notifier) throws IOException {
		boolean alien = dCharacter.isAlien();
		if (alien) {
			Alien a = new Alien(keyNotifier, notifier, alienNum);
			aliens.add(alienNum, a);
			playersRole.put(keyNotifier, true);
						
			notifier.setPlayer(aliens.get(alienNum).getName(), "Alieno");
			
			alienNum++;
			
		} else {
			Human h =  new Human(keyNotifier, notifier, humanNum);
			humans.add(humanNum, h);
			playersRole.put(keyNotifier, false);
			
			notifier.setPlayer(humans.get(humanNum).getName(), "Umano");
			
			humanNum++;
		}
    }
	
	/**
	 * Starts the game. At the start of every "game turn" sets
	 * the counters for both humans and aliens. Let every player play his "character turn".
	 * It cycles game turns until turn 40 or until all humans are dead, then calls "endGame()".
	 * @return Map<playerID, playerName> winners
	 * @throws IOException
	 */
	public Map<Integer, String> playGame() throws IOException {
		do {
			alienIndex = 0;
			humanIndex = 0;
			Map<Integer, Boolean> iterator = new HashMap<Integer, Boolean>(playersRole);
			
			for (Integer i : iterator.keySet()) {
				if (playersRole.containsKey(i)) {
					currentPlayer = (int) i;
					playTurn();
				}
			}
			currentTurn ++;
		} while (currentTurn < MAX_TURN && ! humans.isEmpty());
		
		endGame();
		
		//TODO: la partita è finita, e printo i vincitori dal notifiers che c'è nell'multimatchhandler
		
		return winners;
	}
	
	/**
	 * Checks if "humans" is empty, if not kills all the humans.
	 * @throws IOException
	 */
	public static void endGame() throws IOException {
		if (!humans.isEmpty()) {
			notifyAll("E' finito il 39° turno...");
			for (Human h: humans)
				killPlayer(h);			
	
		}
		notifyAll("LA PARTITA SI E' CONCLUSA!");
	}
	
	
	/**
	 * Check what kind of character is the current player and then let him play his character's turn, and 
	 * increases the proper turn counter.
	 * if he's human after the turn check if he won during his turn and eventually removes him from "humans"
	 * so that he can't be able to play more turns.
	 * @throws IOException
	 */
	public void playTurn() throws IOException {
		boolean isAlien = (boolean) playersRole.get(currentPlayer);
		
		if (isAlien) {                                                  
			aliens.get(alienIndex).playTurn();
			alienIndex ++;
			
		} else {
			humans.get(humanIndex).playTurn();
			if(winners.containsKey(humans.get(humanIndex).getPlayerID()))
				humans.remove(humanIndex);
			humanIndex ++;
		}
	}
	
	/**
	 * Remove a character from "playersRole" and from his "character" list, so that he can't play any more 
	 * turns, announces the death to all players.
	 * If the player who died was the last human, adds all aliens id and names to "winners"
	 * @param p
	 * @throws IOException
	 */
	public static void killPlayer(Player p) throws IOException{
		int playerID = p.getPlayerID();
		boolean playerToKill = (boolean) playersRole.get(playerID);
		int counter = 0;
		String nameDead = p.getName();
		
		for (Integer i : playersRole.keySet()) {
			if((boolean) playersRole.get(i) == playerToKill && i < playerID)
				counter++;
		}
		playersRole.remove(playerID);

		if (playerToKill)
			aliens.remove(counter);
		else
			humans.remove(counter);
		deadPlayers.add((Integer) playerID);
		notifyAll(nameDead + " è stato ucciso.");
		if(humans.isEmpty())
			for(Alien a: aliens)
				addWinner((Integer) playerID, a.getName());
	}
	
	public static void addWinner(Integer id, String newWinner) {
		winners.put(id, newWinner);
	}
	
	/**
	 * Notify a message to ALL the players still playing.
	 * @param message
	 * @throws IOException
	 */
	public static void notifyAll(String message) throws IOException {
		
		for(Alien a: aliens)
			a.getNotifier().write("\n" + message + "\n");
		
		for(Human h: humans)
			h.getNotifier().write("\n" + message + "\n"); 
	}
}
