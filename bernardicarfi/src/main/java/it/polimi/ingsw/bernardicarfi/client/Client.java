package it.polimi.ingsw.bernardicarfi.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;

/**
 * @author Carfì Salvatore, Bernardi Anna
 *
 */

/**
 * Class which is used to instantiate the client.
 * It asks to the user which network interface must be used.
 * This client creates a RMI registry 
 * to be called by the server.
 */
public class Client {
	
	private Client() {
	}
	
	
	/**
	 * Starts the client
	 */
	public static void main(String[] args) throws IOException, NotBoundException {
		//Starting RMI register

		System.out.println("BENVENUTO SU ESCAPE ALIEN FROM OUTER SPACE\n");
		String read = "";
		while(!"2".equals(read)){
			System.out.println("Scegli che interfaccia di rete usare:");
			System.out.println("1 - Socket (Coming soon)");
			System.out.println("2 - RMI");
			read = readLine("");
			if(!"1".equals(read)  && !"2".equals(read))
				System.out.println("Comando non riconosciuto!");
			if("1".equals(read))
				System.out.println("L'interfacciadi rete selezionata non è disponibile.");
		}
		
		System.out.println("Attendi gli altri giocatori...");

		//Factory method to create the interface.
		NetworkInterface ni = NetworkInterfaceFactory.getInterface(read);
		
		//using the interface
		ni.connect();
			
		//Loop to query/stop the client
		boolean end = false;
		while(!end){
			read = readLine("");
			if("Q".equals(read)) {
				while(!"1".equals(read)  && !"2".equals(read)){
					System.out.println("Sei sicuro? Una volta disconnesso dalla partita non potrai più accedervi");
					System.out.println("1 - Si");
					System.out.println("2 - Annulla");
					read = readLine("");
					if(!"1".equals(read)  && !"2".equals(read))
						System.out.println("Comando non riconosciuto!");
				}
				if ("1".equals(read))
						end = true;
			}
		}
		
		if(ni.close()){
			System.out.println("Ti sei disconnesso dalla partita.");
		}
		System.exit(0);
	}
	
	/**
	 * Helper method to read a line from the console when the 
	 * program is started in Eclipse
	 * @param format the format string to be read
	 * @param args arguments for the format
	 * @return the read string
	 * @throws IOException if the program cannot access the stdin
	 */
	private static String readLine(String format, Object... args) throws IOException {
	    if (System.console() != null) {
	        return System.console().readLine(format, args);
	    }
	    System.out.print(String.format(format, args));
	    
	    BufferedReader br = null;
	    InputStreamReader isr = null;
	    String read = null;
	    
	    isr = new InputStreamReader(System.in);
	    br = new BufferedReader(isr);
	    read = br.readLine();
	    
	    System.out.println();

	    return read;
	}
}