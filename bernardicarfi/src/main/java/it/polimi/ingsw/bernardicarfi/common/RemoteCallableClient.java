package it.polimi.ingsw.bernardicarfi.common;

import it.polimi.ingsw.bernardicarfi.server.CallableClient;
import it.polimi.ingsw.bernardicarfi.server.RemoteNotifier;
import it.polimi.ingsw.bernardicarfi.server.Server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @author Salvatore Carfì, Anna Bernardi
 *
 */

/**
 * Class which implements the CallableClient interface.
 * It allows the server to call the remote client.
 */
public class RemoteCallableClient implements CallableClient {
	private Server server;
	
	@Override
	public void setClientPort(int port, int playerNum) throws RemoteException {
		this.server = Server.getServer();
		Registry registry;
				
		try {
			//Locate the registry and get the remote notifier
			registry = LocateRegistry.getRegistry(port);
			
			RemoteNotifier rn = (RemoteNotifier)registry.lookup("Notifier");
			server.addNotifier(playerNum, rn);
			
		} catch (Exception ex){
		}
	}
}
