package it.polimi.ingsw.bernardicarfi.common;

import java.io.IOException;
import java.util.List;

/**
 * @author Salvatore Carfì, Anna Bernardi
 * This class is the general View interface.
 */

public interface View {
	
	public String endTurn();
	public String getSector(List<String> moves) throws IOException;
	public void write(String s) throws IOException;
	public String getAnySector() throws IOException;
	public boolean yesNo(String s) throws IOException;
	public int askCard(List<String> cards) throws IOException;
	public boolean useDiscard() throws IOException;
	public void printGrid() throws IOException;
	public void setPlayerName(String name, String role) throws IOException;
	public String getType();
}