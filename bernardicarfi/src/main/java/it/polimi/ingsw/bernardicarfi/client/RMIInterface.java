package it.polimi.ingsw.bernardicarfi.client;

import it.polimi.ingsw.bernardicarfi.server.CallableClient;
import it.polimi.ingsw.bernardicarfi.server.RemoteNotifier;
import it.polimi.ingsw.bernardicarfi.server.rmi.RemoteRMIHandler;

import java.io.IOException;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author Carfì Salvatore, Bernardi Anna
 *
 */

/**
 * Class that implements the NetworkInterface.
 * It implements all the methods such that they can communicate
 * with the server through the RMI protocol.
 */
public class RMIInterface implements NetworkInterface {
	
	private static final int STANDARD_PORT = 3030;
	private RemoteRMIHandler rmiHandler;
	private static RemoteNotifier notifier;
	private static RemoteNotifier stub;
	private static Registry registry = null;
	private static final String NOT_NAME = "Notifier";
	private int portNumber;
    private int playersNum;
    private int matchNum;
	
	/**
	 * Connects the client to server, with the RMI method
	 */
	@Override
	public boolean connect() throws IOException {
		String name = "RMIHandler";
		String clientName = "Client";
        Registry serverRegistry;
        
		try {
			//Get the Registry
			serverRegistry = LocateRegistry.getRegistry(2020);
			
		} catch (RemoteException e) {			
			return false;
		}
		
		try {
			//Start the RMIHandler registry
        	rmiHandler = (RemoteRMIHandler) serverRegistry.lookup(name);
			
        	playersNum = rmiHandler.getPlayersNum();
        	matchNum = rmiHandler.getIndex();
        	//System.out.println("playersnum " + playersNum + "matchNums " + matchNum);
    		this.portNumber = STANDARD_PORT + playersNum + 8 * matchNum;
    		
            //Binding the notifier
            notifier = new Notifier();
            stub = (RemoteNotifier) UnicastRemoteObject.exportObject(notifier, 0); 

            registry = LocateRegistry.createRegistry(portNumber);
            registry.bind(NOT_NAME, stub);
            
        } catch (Exception e) {
            System.err.println("RMI exception:");
            e.printStackTrace();
            registry = null;
        }
		
        try {
        	//Register the client to the server.
        	((CallableClient) serverRegistry.lookup(clientName)).setClientPort(portNumber, playersNum);
        	
		} catch (AccessException e) {
			return false;
		} catch (RemoteException e) {
			return false;
		} catch (NotBoundException e) {
			return false;
		}
                
        
        rmiHandler.addPlayer();
        rmiHandler.startMatchIfReady();
		return true;
	}

	@Override
	public boolean close() {
		//TODO: come faccio a far andare avanti il gioco se disconnetto un giocatore? come risalgo al thread che ha creato il match
		//      per farlo andare avanti (anche se il gioco va avanti quello rimane aperto per sempre ?) ? come rompo il ciclo del 
		//      playTurn dall' "esterno" (nel caso in cui il giocatore si disconnette durante nel mel suo turno) ?
		return true;
	}

}