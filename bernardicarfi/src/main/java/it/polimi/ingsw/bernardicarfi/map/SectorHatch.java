package it.polimi.ingsw.bernardicarfi.map;

import it.polimi.ingsw.bernardicarfi.Match;
import it.polimi.ingsw.bernardicarfi.decks.DeckEscapeHatch;
import it.polimi.ingsw.bernardicarfi.players.Player;

import java.io.IOException;

/**
 * @author Salvatore Carfì
 * 
 */

public class SectorHatch extends Sector {

	private boolean used;
	
	public SectorHatch(int q, int r, int s) {
		super(q, r, s);
		used = false;
	}
	
	/**
	 * When a human walks in it check if the hatch has already been used, if not, let him draw an hatch card
	 * if the hatch card isn't damaged (false) add the player to winners.
	 */
	@Override
	public void walkIn(Player p) throws IOException {
		super.walkIn(p);
		if (!used && p.getType() == "human") {
			if (!DeckEscapeHatch.isDamaged()) {
				Match.addWinner((Integer) p.getPlayerID(), p.getName());
			}
			used = true;
		}
	}
}
