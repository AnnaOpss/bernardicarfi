package it.polimi.ingsw.bernardicarfi.server.rmi;

import it.polimi.ingsw.bernardicarfi.Match;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Salvatore Carfì, Anna Bernardi
 * 	Interface for the RMIHandler.
 */

public interface RemoteRMIHandler extends Remote {

	public void startMatchIfReady() throws IOException;
	
	public void addPlayer() throws IOException;
	
	public int getPlayersNum() throws RemoteException;

	public int getIndex() throws RemoteException;

	public Match getMatch(int matchNum) throws RemoteException;
	
}
