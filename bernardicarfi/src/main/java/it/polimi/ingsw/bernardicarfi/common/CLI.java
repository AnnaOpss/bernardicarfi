package it.polimi.ingsw.bernardicarfi.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * @author Carfì Salvatore, Bernardi Anna
 *
 */

public class CLI implements View {
	
	private static final String CMD = "Comando non riconosciuto";
	private static final String ANSI_RESET = "\u001B[0;1m";
	private static final String ANSI_RED = "\u001B[31;1m";
	private static final String ANSI_BLUE = "\u001B[34;1m";
	private static final String ANSI_CYAN = "\u001B[36;1m";
	
	/**
	 * Notify the player he ended his turn.
	 */
	@Override
	public String endTurn() {
		System.out.println("Turno finito.");
		return "Turn ended";
	}
	
	/**
	 * Show a player the possible moves he can make, showing him the name of the sectors he can reach. 
	 * If the sector he asked for exists, return his answer as a String.
	 */
	@Override
	public String getSector(List<String> moves) throws IOException {
		System.out.println("Scegli la mossa da fare:\n" + moves.toString() );
		String s = "";
		do {
			System.out.println("Inserisci il nome di un settore:");
			s = readLine("");
			
			if(!isSector(s))
				System.out.println("Settore non riconosciuto!");
			//System.out.println(GameMap.typeSector(Sector.nameToSector(s)));
		} while(!isSector(s));

		return s;
	}
	
	/**
	 * Ask the player to choose any sector.
	 */
	@Override
	public String getAnySector() throws IOException {
		String s = "";
		do {
			System.out.println("Inserisci il nome di un settore:");
			s = readLine("");
		
			if(!isSector(s))
				System.out.println("Settore non riconosciuto!");
			//System.out.println(GameMap.typeSector(Sector.nameToSector(s)));
		} while(!isSector(s));

	return s;
}
	/**
	 * Check if "s" is a valid string for a sector, contained in the map.
	 * @param s is a String with the name of a sector.
	 * @return true, if the sector is contained in the map, false if is not.
	 */
	private boolean isSector(String s) {
		boolean valid = false;
		try {
			if ((int) s.charAt(0) <= (int) 'W'
			&& (int) s.charAt(0) >= (int) 'A'
			&& Integer.parseInt(s.substring(1)) >=1
			&& Integer.parseInt(s.substring(1)) <= 14)
			valid = true;
		} catch (StringIndexOutOfBoundsException e1) {
			return false;
		} catch (NumberFormatException e2) {
			return false;
		}
		return valid;
	}
	
	/**
	 * Write the string s to the player.
	 */
	@Override
	public void write (String s) throws IOException {
		System.out.println(s);	
	}
	
	/**
	 * Print the map.
	 */
	@Override
	public void printGrid() throws IOException {
		System.out.println("Per vedere la griglia colorata installare il seguente plugin: \nhttps://marketplace.eclipse.org/content/ansi-escape-console");
		System.out.println(ANSI_RESET + ANSI_RED + " \n  ___     ___             ___     ___     ___     ___             ___             ___     ___  \n /A01\\___/" + ANSI_RESET + "C01" + ANSI_RED + "\\        ___/G01\\___/" + ANSI_RESET + "I01" + ANSI_RED + "\\___/K01\\___/M01\\___     ___/" + ANSI_RESET + "Q01" + ANSI_RED + "\\___        /" + ANSI_RESET + "U01" + ANSI_RED + "\\___/W01\\ \n \\___/B01\\___/    ___/" + ANSI_RESET + "F01" + ANSI_RED + "\\___/" + ANSI_RESET + "H01" + ANSI_RED + "\\___/" + ANSI_RESET + "J01" + ANSI_RED + "\\___/L01\\___/N01\\___/" + ANSI_RESET + "P01" + ANSI_RED + "\\___/" + ANSI_RESET + "R01" + ANSI_RED + "\\___    \\___/" + ANSI_RESET + "V01" + ANSI_RED + "\\___/ \n /A02\\___/C02\\___/" + ANSI_RESET + "E02" + ANSI_RED + "\\___/G02\\___/I02\\___/" + ANSI_RESET + "K02" + ANSI_RED + "\\___/" + ANSI_RESET + "M02" + ANSI_RED + "\\___/O02\\___/Q02\\___/S02\\___/U02\\___/W02\\ \n \\___/ " + ANSI_CYAN + "1" + ANSI_RED + " \\___/D02\\___/F02\\___/" + ANSI_RESET + "H02" + ANSI_RED + "\\___/J02\\___/" + ANSI_RESET + "L02" + ANSI_RED + "\\___/N02\\___/P02\\___/R02\\___/T02\\___/ " + ANSI_CYAN + "2" + ANSI_RED + " \\___/ \n /A03\\___/C03\\___/E03\\___/G03\\___/I03\\___/K03\\___/M03\\___/   \\___/Q03\\___/   \\___/U03\\___/" + ANSI_RESET + "W03" + ANSI_RED + "\\ \n \\___/B03\\___/D03\\___/F03\\___/" + ANSI_RESET + "H03" + ANSI_RED + "\\___/J03\\___/L03\\___/" + ANSI_RESET + "N03" + ANSI_RED + "\\   /" + ANSI_RESET + "P03" + ANSI_RED + "\\___/R03\\___    \\___/V03\\___/ \n /" + ANSI_RESET + "A04" + ANSI_RED + "\\___/C04\\___/E04\\___/G04\\___/I04\\___/K04\\___/M04\\___/   \\___/" + ANSI_RESET + "Q04" + ANSI_RED + "\\___/S04\\   /U04\\___/" + ANSI_RESET + "W04" + ANSI_RED + "\\ \n \\___/B04\\___/   \\___/F04\\___/H04\\___/J04\\___/" + ANSI_RESET + "L04" + ANSI_RED + "\\___/N04\\___/" + ANSI_RESET + "P04" + ANSI_RED + "\\___/" + ANSI_RESET + "R04" + ANSI_RED + "\\___/   \\___/V04\\___/ \n /" + ANSI_RESET + "A05" + ANSI_RED + "\\___/C05\\___/E05\\___/G05\\___/I05\\___/" + ANSI_RESET + "K05" + ANSI_RED + "\\___/" + ANSI_RESET + "M05" + ANSI_RED + "\\___/" + ANSI_RESET + "O05" + ANSI_RED + "\\___/Q05\\___/S05\\___/" + ANSI_RESET + "U05" + ANSI_RED + "\\___/" + ANSI_RESET + "W05" + ANSI_RED + "\\ \n \\___/" + ANSI_RESET + "B05" + ANSI_RED + "\\___/D05\\___/F05\\___/   \\___/J05\\___/L05\\___/N05\\___/P05\\___/R05\\___/T05\\___/V05\\___/ \n /" + ANSI_RESET + "A06" + ANSI_RED + "\\___/C06\\___/E06\\___/G06\\___    \\___/K06\\___/M06\\___/O06\\___/" + ANSI_RESET + "Q06" + ANSI_RED + "\\___/S06\\___/U06\\___/" + ANSI_RESET + "W06" + ANSI_RED + "\\ \n \\___/B06\\___/   \\___/F06\\___/H06\\___/J06\\___/" + ANSI_BLUE + "AAA" + ANSI_RED + "\\___/N06\\___/P06\\___/" + ANSI_RESET + "R06" + ANSI_RED + "\\___/T06\\___/V06\\___/ \n     \\___/C07\\       \\___/" + ANSI_RESET + "G07" + ANSI_RED + "\\___/I07\\___/   \\___/   \\___/O07\\___/Q07\\___/S07\\___/U07\\___/     \n         \\___/    ___/F07\\___/" + ANSI_RESET + "H07" + ANSI_RED + "\\___/    ___     ___    \\___/P07\\___/" + ANSI_RESET + "R07" + ANSI_RED + "\\___/" + ANSI_RESET + "T07" + ANSI_RED + "\\___/    ___  \n      ___/C08\\___/E08\\___/G08\\___/I08\\___/K08\\___/M08\\___/O08\\___/Q08\\___/S08\\___/U08\\___/W08\\ \n  ___/B08\\___/D08\\___/F08\\___/H08\\___/J08\\___/" + ANSI_BLUE + "UUU" + ANSI_RED + "\\___/N08\\___/P08\\___/" + ANSI_RESET + "R08" + ANSI_RED + "\\___/" + ANSI_RESET + "T08" + ANSI_RED + "\\___/" + ANSI_RESET + "V08" + ANSI_RED + "\\___/ \n /" + ANSI_RESET + "A09" + ANSI_RED + "\\___/C09\\___/E09\\___/G09\\___/" + ANSI_RESET + "I09" + ANSI_RED + "\\___/" + ANSI_RESET + "K09" + ANSI_RED + "\\___/" + ANSI_RESET + "M09" + ANSI_RED + "\\___/" + ANSI_RESET + "O09" + ANSI_RED + "\\___/Q09\\___/S09\\___/U09\\___/W09\\ \n \\___/B09\\___/D09\\___/F09\\___/H09\\___/J09\\___/" + ANSI_RESET + "L09" + ANSI_RED + "\\___/N09\\___/P09\\___/R09\\___/   \\___/V09\\___/ \n /" + ANSI_RESET + "A10" + ANSI_RED + "\\___/C10\\___/E10\\___/G10\\___/I10\\___/K10\\___/M10\\___/O10\\___/Q10\\___/       /U10\\___/" + ANSI_RESET + "W10" + ANSI_RED + "\\ \n \\___/" + ANSI_RESET + "B10" + ANSI_RED + "\\___/" + ANSI_RESET + "D10" + ANSI_RED + "\\___/" + ANSI_RESET + "F10" + ANSI_RED + "\\___/   \\___/J10\\___/L10\\___/N10\\___/P10\\___/           \\___/V10\\___/ \n /" + ANSI_RESET + "A11" + ANSI_RED + "\\___/C11\\___/E11\\___/G11\\   /I11\\___/" + ANSI_RESET + "K11" + ANSI_RED + "\\___/" + ANSI_RESET + "M11" + ANSI_RED + "\\___/O11\\___/" + ANSI_RESET + "Q11" + ANSI_RED + "\\        ___/U11\\___/" + ANSI_RESET + "W11" + ANSI_RED + "\\ \n \\___/B11\\___/D11\\___/F11\\___/   \\___/J11\\___/" + ANSI_RESET + "L11" + ANSI_RED + "\\___/N11\\___/P11\\___/    ___/T11\\___/V11\\___/ \n /" + ANSI_RESET + "A12" + ANSI_RED + "\\___/C12\\___/" + ANSI_RESET + "E12" + ANSI_RED + "\\___/" + ANSI_RESET + "G12" + ANSI_RED + "\\___    \\___/K12\\___/M12\\___/O12\\___/Q12\\___/S12\\___/" + ANSI_RESET + "U12" + ANSI_RED + "\\___/" + ANSI_RESET + "W12" + ANSI_RED + "\\ \n \\___/B12\\___/D12\\___/   \\___/H12\\___    \\___/L12\\___/N12\\___/" + ANSI_RESET + "P12" + ANSI_RED + "\\___/" + ANSI_RESET + "R12" + ANSI_RED + "\\___/T12\\___/V12\\___/ \n /" + ANSI_RESET + "A13" + ANSI_RED + "\\___/C13\\___/E13\\   /G13\\___/I13\\___    \\___/M13\\___/O13\\___/Q13\\___/S13\\___/U13\\___/W13\\ \n \\___/ " + ANSI_CYAN + "4" + ANSI_RED + " \\___/D13\\___/   \\___/H13\\___/J13\\___/L13\\___/N13\\___/P13\\___/R13\\___/T13\\___/ " + ANSI_CYAN + "3" + ANSI_RED + " \\___/ \n /A14\\___/" + ANSI_RESET + "C14" + ANSI_RED + "\\___/       /" + ANSI_RESET + "G14" + ANSI_RED + "\\___/" + ANSI_RESET + "I14" + ANSI_RED + "\\___/K14\\___/M14\\___/" + ANSI_RESET + "O14" + ANSI_RED + "\\___/" + ANSI_RESET + "Q14" + ANSI_RED + "\\___/   \\___/U14\\___/W14\\ \n \\___/B14\\___/" + ANSI_RESET + "D14" + ANSI_RED + "\\       \\___/" + ANSI_RESET + "H14" + ANSI_RED + "\\___/" + ANSI_RESET + "J14" + ANSI_RED + "\\___/" + ANSI_RESET + "L14" + ANSI_RED + "\\___/" + ANSI_RESET + "N14" + ANSI_RED + "\\___/P14\\___/R14\\   /" + ANSI_RESET + "T14" + ANSI_RED + "\\___/V14\\___/ \n     \\___/   \\___/           \\___/   \\___/   \\___/   \\___/   \\___/   \\___/   \\___/   \\___/     \n " + ANSI_RESET);
	}
	
	/**
	 * Show a question with 2 choices to a player. 
	 * @return true if the answer is 1, false if 2.
	 */
	@Override
	public boolean yesNo(String question) throws IOException {
		String answer; 
		boolean res = false;
		do {
			answer = readLine(question + "\n");
			if ("1".equals(answer))
				res = true;

			if (!("1".equals(answer) || "2".equals(answer)))
				write (CMD);
			
		} while (!("1".equals(answer) || "2".equals(answer)));
		return res;
	}
	
	
	@Override
	public boolean useDiscard() throws IOException {
		String answer = readLine("1 - USA\n2 - SCARTA\n");
		boolean res = false;
		do {
			if ("1".equals(answer))
				res = true;

			if (!("1".equals(answer) || "2".equals(answer)))
				write (CMD);
			
		} while (!("1".equals(answer) || "2".equals(answer)));
		return res;
	}
	
	/**
	 * Print a list of card (which is the list of cards in the player's hand) and ask him to choose one.
	 */
	@Override
	public int askCard(List<String> cards) throws IOException {
		System.out.println("Scegli il numero della carta dalla tua mano");
		
		int i = 1;
		int input;
		
		do {

			for (String item : cards) {
				System.out.println(i + " - " + item);
				i ++;
			}
		
			String s = readLine("");
			input = Integer.parseInt(s);
			
			if (!(input <= cards.size() && input > 0))
				write (CMD);
			
		} while (!(input <= cards.size() && input > 0));
		
		return input - 1;
	}
	
	/**
	 * Get the name of the graphic interface on use.
	 * @return "CLI"
	 */
	@Override
	public String getType()  {
		return "CLI";
	}

	/**
	 * Show the player his character and the name of it.
	 */
	@Override
	public void setPlayerName(String name, String role) throws IOException {
		System.out.println("Sei un " + role + ".\nSei " + name);
	}
	
	private static String readLine(String format, Object... args) throws IOException {
	    if (System.console() != null) {
	        return System.console().readLine(format, args);
	    }
	    System.out.print(String.format(format, args));
	    
	    BufferedReader br = null;
	    InputStreamReader isr = null;
	    String read = null;
	    
	    isr = new InputStreamReader(System.in);
	    br = new BufferedReader(isr);
	    read = br.readLine();
	    
		System.out.println();

	    return read;
	}
}