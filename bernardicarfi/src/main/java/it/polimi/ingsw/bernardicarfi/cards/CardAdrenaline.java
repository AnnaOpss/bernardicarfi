package it.polimi.ingsw.bernardicarfi.cards;

import it.polimi.ingsw.bernardicarfi.players.Player;

/**
 * @author Carfì Salvatore
 *
 */

public class CardAdrenaline extends Card {
	
	/**
	 * Set step to 2 on player
	 */
	@Override
	public void activateCard(Player p) {
		p.setStep(2);
	}

	@Override
	public String name() {
		return "Adrenalina";
	}
	
}
