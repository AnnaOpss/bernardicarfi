package it.polimi.ingsw.bernardicarfi.map;

import it.polimi.ingsw.bernardicarfi.players.Player;

import java.io.IOException;

/**
 * @author Salvatore Carfì, Anna Bernardi
 * 
 */

public class SectorBlocked extends Sector {

	public SectorBlocked(int q, int r, int s) {
		super(q, r, s);
	}
	
	/**
	 * This method should never be called.
	 */
	@Override
	public void walkIn(Player p) throws IOException {
		p.getNotifier().write("Settore non accessibile.");
	}
}
