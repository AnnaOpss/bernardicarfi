package it.polimi.ingsw.bernardicarfi.map;

import it.polimi.ingsw.bernardicarfi.Match;
import it.polimi.ingsw.bernardicarfi.players.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bernardi Anna, Carfi Salvatore
 *
 */

public class Sector {
	
	/**
	 * The six possible directions in Sector. Add one of those to a
	 * Sector and you get one of its neighbors.
	 */
	private static final Sector[] DIRECTIONS_EVEN = new Sector[] {
			new Sector(1, -1, 0), new Sector(1, 0, -1),
			new Sector(0, 1, -1), new Sector(-1, -1, -2),
			new Sector(-1, 0, 1), new Sector(0, -1, 1) };
	
	private static final Sector[] DIRECTIONS_ODD = new Sector[] {
		new Sector(1, 1, -2), new Sector(1, 0, -1),
		new Sector(0, 1, -1), new Sector(-1, 1, 0),
		new Sector(-1, 0, 1), new Sector(0, -1, 1) };
	
	private List<Player> playersHere = new ArrayList<Player>();

	private final int q;
    private final int r;
    private final int s;
	
    public Sector(int q, int r, int s) {
        this.q = q;
        this.r = r;
        this.s = s;
    }
   
    
	public List<String> reachable(int movement) {
    	List<String> visited = new ArrayList<String>();
    	List<Sector> tmp = new ArrayList<Sector>();
    	List<Sector> centers = new ArrayList<Sector>();
    	centers.add(this);

    	int step = 0;
    	String start = this.sectorToName();
    	
    	while (step < movement) {
    		for (Sector centre : centers) {
    			List<Sector> otherNeighbors = centre.getNeighbors();

    	    	for (Sector theNeighbor : otherNeighbors) {
    	    		String name = theNeighbor.sectorToName();
    	    		
    	    		if (!visited.contains(name)
    	    			&& !name.equals(start)
    	    			&& GameMap.typeSector(theNeighbor) != "Blocked" 
    	    			&& GameMap.typeSector(theNeighbor) != "alien_start"
    	    			&& GameMap.typeSector(theNeighbor) != "human_start") {   	    			
    	    					
    	    			if (theNeighbor.r >= 0 && theNeighbor.r < 14 
    	    				&& theNeighbor.q >= 0 && theNeighbor.q < 23) {
    	    				
    	    				visited.add(name);
    	    				tmp.add(theNeighbor);
    	    			}
    	    		}
    	    	}
    		}
    		
    		step ++;
    		centers = new ArrayList<Sector>(tmp);
    		tmp = new ArrayList<Sector>();
    	}
    	return visited;
    }
    
    /**
	 * @return A new Sector gotten by adding the given Sector
	 *         component by component
	 */
    public Sector add(Sector a, Sector b) {
        return new Sector(a.q + b.q, a.r + b.r, a.s + b.s);
    }
  
	/**
	 * @return Sector for the six neighbors of this CubeCoordinate
	 */
	public List<Sector> getNeighbors() {
		List<Sector> neighbors = new ArrayList<Sector>(6);
		int coloumn = this.q;
		Sector tmp;
		
		for (int i = 0; i < 6; i++){
			if (coloumn % 2 == 0) {
				tmp = this.add(this, DIRECTIONS_EVEN[i]);
			} else {
				tmp = this.add(this, DIRECTIONS_ODD[i]);
			}
			
			if (tmp.r >= 0 && tmp.r < 14 && tmp.q >= 0 && tmp.q < 23)
				neighbors.add(tmp);
		}
		return neighbors;
	}

	/**
	 * @return whether the passed Sector is a neighbor of this one
	 */
	public boolean isNeighbor(Sector sec) {
		return Math.abs(sec.q - this.q) <= 1
				&& Math.abs(sec.r - this.r) <= 1
				&& Math.abs(sec.s - this.s) <= 1
				&& !this.equals(sec);
	}

	public void walkIn(Player p) throws IOException {
		playersHere.add(p);
	}
	
	public void walkOut(Player p) {
		playersHere.remove(p);
	}
	
	/**
	 * Kills all the players in this sector except the one who attacked (p)
	 * @param p
	 * @return true if someone died, false if not.
	 * @throws IOException 
	 */
	public boolean attackFrom(Player p) throws IOException {
		boolean killedHuman = false;
		for (Player pKilled : playersHere){
			if(!(pKilled.equals(p))) {
				if(pKilled.death() && pKilled.getType() == "human")
					killedHuman = true;
				playersHere.remove(pKilled);
			}
		}
		return killedHuman;
	}
	
	/**
	 * Notify to all players the players in this sector.
	 * @throws IOException
	 */
	public void showPlayersHere() throws IOException{
		Match.notifyAll("Nel settore " + this.sectorToName() + " ci sono i seguenti giocatori:");
		for (Player p: playersHere) {
			Match.notifyAll("Giocatore " + p.getPlayerID());
		}
	}
	
	public String getCharForNumber(int i) {
	    return i >= 0 && i < 27 ? String.valueOf((char)(i + 'A')) : null;
	}
	
	private static int getNumberForChar(char s) {
		return (int) s - (int)'A';
	}
	
	/**
	 * Convert a sector to its name
	 * @return name of a sector
	 */
	public String sectorToName(){
		return getCharForNumber(this.q) + String.valueOf(this.r + 1);
	}
	
	/**
	 * Convert a name to a sector.
	 * @param name
	 * @return the sector, given its name
	 */
	public static Sector nameToSector(String name) {
		int q = 0;
		int r = 0;
		
		try {
			q = getNumberForChar(name.charAt(0));
			r = Integer.parseInt(name.substring(1)) - 1;
			
		} catch (StringIndexOutOfBoundsException e1) {
			System.out.println("E' stato rilevato un errore, la coordinata X verrà settata ad 'A'");
			q = 0;
		} catch (NumberFormatException e2) {
			System.out.println("E' stato rilevato un errore, la coordinata Y verrà settata a '1'");
			r = 0;
		}

		return GameMap.sectorToGrid(new Sector(q, r, -q-r));
	}
	
	public int getQ() {
		return this.q;
	}
	
	public int getR() {
		return this.r;
	}
	
	public int getS() {
		return this.s;
	}
}
