package it.polimi.ingsw.bernardicarfi.client;

import java.io.IOException;

/**
 * @author Carfì Salvatore, Bernardi Anna
 *
 */

/**
 * Interface tht exposes all the methods that must be 
 * implemented by all the available network interfaces
 */
public interface NetworkInterface {
	/**
	 * Method that connects the client to the server
	 * @return <b>true</b> if the connection is established successfully; <b>false</b> otherwise.
	 * @throws IOException
	 */
	boolean connect() throws IOException;
	/**
	 * Method that closes the connection
	 * @return <b>true</b> if the connection is closed successfully; <b>false</b> otherwise.
	 * @throws IOException
	 */
	boolean close() throws IOException;
	
}
