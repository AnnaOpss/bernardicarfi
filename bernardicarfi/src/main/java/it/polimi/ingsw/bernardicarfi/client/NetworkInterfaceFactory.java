package it.polimi.ingsw.bernardicarfi.client;

public class NetworkInterfaceFactory {

/**
 * @author Carfì Salvatore, Bernardi Anna
 *
 */
	
	private NetworkInterfaceFactory(){
		
	}
	
	/**
	 * Static method that creates the method interface.
	 * @param param <b>1</b> to create a Socket interface; <b>2</b> to create an RMI interface.
	 * @return the required NetworkInterface
	 */
	public static NetworkInterface getInterface(String param){
		return new RMIInterface();
	}
}
