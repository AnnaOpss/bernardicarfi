package  it.polimi.ingsw.bernardicarfi.cards;

import java.io.IOException;
import java.rmi.RemoteException;

import it.polimi.ingsw.bernardicarfi.players.Player;

/**
 * @author Bernardi Anna, Carfi Salvatore
 * General interface for cards
 */

public abstract class Card {
	
	/**
	 * @return a string with the name of the card
	 */
	public abstract String name();
	
	/**
	 * Activates a card, overrided by each one to apply the specific effect.
	 */
	public abstract void activateCard(Player p) throws RemoteException, IOException;
}
