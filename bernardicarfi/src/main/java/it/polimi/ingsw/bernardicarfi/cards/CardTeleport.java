package it.polimi.ingsw.bernardicarfi.cards;

import java.io.IOException;

import it.polimi.ingsw.bernardicarfi.map.GameMap;
import it.polimi.ingsw.bernardicarfi.players.Player;

/**
 * @author Carfì Salvatore
 *
 */

public class CardTeleport extends Card {

	/**
 	* @author Carfì Salvatore
 	* Makes a player teleport into "human sector"-
 	*/
	@Override
	public void activateCard(Player p) throws IOException {
		p.moveTo(GameMap.humanSectorLocation());
	}
	
	@Override
	public String name() {
		return "Teletrasporto";
	}
	
}
