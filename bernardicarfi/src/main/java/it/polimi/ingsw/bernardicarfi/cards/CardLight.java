package it.polimi.ingsw.bernardicarfi.cards;

import java.io.IOException;
import java.util.List;

import it.polimi.ingsw.bernardicarfi.map.GameMap;
import it.polimi.ingsw.bernardicarfi.map.Sector;
import it.polimi.ingsw.bernardicarfi.players.Player;

/**
 * @author Carfì Salvatore
 *
 */
public class CardLight extends Card {
	
	/**
	 * Ask a sector to the player, then shows the players in that sector and in near ones to every player.  
	 */
	@Override
	public void activateCard(Player p) throws IOException {
		String positionName = p.getNotifier().askAnySector();	
		Sector position = Sector.nameToSector(positionName);
		List<Sector> neighbors = position.getNeighbors();
		
		position.showPlayersHere();
		
		for (Sector s : neighbors)
			GameMap.sectorToGrid(s).showPlayersHere();
	}
	
	@Override
	public String name() {
		return "Spotlight";
	}

}
