package it.polimi.ingsw.bernardicarfi.cards;

import java.io.IOException;

import it.polimi.ingsw.bernardicarfi.Match;
import it.polimi.ingsw.bernardicarfi.map.GameMap;
import it.polimi.ingsw.bernardicarfi.map.Sector;
import it.polimi.ingsw.bernardicarfi.players.Player;

/**
 * @author Carfì Salvatore
 *
 */

public class CardNoiseAnywhere extends Card {
	
	/**
	 * Ask a sector to the player, then shows the players in that sector and in near ones to every player.  
	 */
	@Override
	public void activateCard(Player p) throws IOException {
		String s;
		boolean validSector;
		do{ 
			validSector = true;
			p.getNotifier().write("Scegli il settore in cui far rumore");
			s = p.getNotifier().askAnySector();
			String type = GameMap.typeSector(Sector.nameToSector(s));
			if ( type == "Blocked" || type == "human_start" || type == "alien_start"){
				p.getNotifier().write("Il settore scelto non è valido");
				validSector = false;
			}
		} while(!validSector);
		Match.notifyAll(p.getName() + ": RUMORE IN " + s);	
	}
	
	@Override
	public String name() {
		return "Rumore in un altro settore";
	}
}
