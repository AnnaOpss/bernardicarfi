package it.polimi.ingsw.bernardicarfi.decks;

import java.util.Random;

/**
 * @author Bernardi Anna
 *
 */

public class DeckEscapeHatch {

	private static Random random;
	private static int TOTAL_HATCHESBroken = 3;
	private static int TOTAL_HATCHESUnbroken = 3;
	private static int usedHatches = 0;
	private static final int TOTAL_HATCHES = 4;
	
	public DeckEscapeHatch(){
    	random = new Random();
    }
	
	protected static boolean getRandomBoolean() {
        return random.nextBoolean();
    }
	
	/**
	 * This function chooses randomly the status of the hatch. 
	 * If status is true, the hatch is damaged.
	 * Before returning the status, it decreases the total number 
	 * of hatches broken or not in order to have the right balance.
	 * Moreover it keeps track of the number of hatches used during the game.
	 */
	public static boolean isDamaged() {
		usedHatches ++;
		
		if (TOTAL_HATCHESBroken == 0) {
			TOTAL_HATCHESUnbroken --;
			return false;
		} else if (TOTAL_HATCHESUnbroken == 0){
			TOTAL_HATCHESBroken --;
			return true;
		}
		
		boolean status = getRandomBoolean();
		
		if (status) {
			TOTAL_HATCHESBroken --;
		} else {
			TOTAL_HATCHESUnbroken --;
		}
		return status;
	}
	
	/**
	 * @return number of active hatches.
	 */
	public static int leftHatches() {
		return TOTAL_HATCHES - usedHatches;
	}
}
