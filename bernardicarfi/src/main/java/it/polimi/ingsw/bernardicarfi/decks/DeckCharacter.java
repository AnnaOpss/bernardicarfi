package it.polimi.ingsw.bernardicarfi.decks;

import java.util.Random;

/**
 * @author Bernardi Anna
 *
 */

public class DeckCharacter {

    private Random random;
    private int totalAlien;
    private int totalHuman;

    public DeckCharacter(int playersNum){
    	random = new Random();
    	rolesPercentage(playersNum);
    }
    
    private boolean getRandomBoolean() {
        return random.nextBoolean();
    }
    
    /**
     * This method sets the global variables total_alien and total_human
	 * at the right value for the number of players.
	 */
	public void rolesPercentage(int playersNumber) {
		totalAlien = playersNumber / 2;
		totalHuman = playersNumber / 2;
		
		if (playersNumber % 2 != 0) { //if players_number is odd there will be one more alien
			totalAlien ++;
		}
	}
	
	/**
		 * This method chooses randomly the role of the player. 
		 * If it's true the player will be an alien, otherwise a human.
		 * Before returning the role, it decreases the total number 
		 * of alien or human in order to have the right balance.
		 */
	public boolean isAlien() {
		if (totalAlien == 0) { //if there are no alien role left the player is human
			totalHuman --;
			return false;
		} else if (totalHuman == 0) {
			totalAlien --;
			return true;
		}
		
		Boolean role = getRandomBoolean();
		
		if (role) {
			totalAlien --;
		} else {
			totalHuman --;
		}
		
		return role;
	}
	
	public int getTotalAlien(){
		return totalAlien;
	}
	
	public int getTotalHuman(){
		return totalHuman;
	}

}
