package it.polimi.ingsw.bernardicarfi.map;

import java.util.Arrays;

/**
 * @author Bernardi Anna, Carfì Salvatore
 *
 */
public class GameMap {

	public static final int HEIGHT = 14;
	public static final int WIDTH = 23;

	private static final Integer[] BLOCKED = {0, 26, 50, 95, 138, 
		139, 141, 161, 164, 168, 175, 188, 215, 221, 225, 226, 239, 
		244, 263, 264, 271, 272, 275, 283, 288, 289, 295, 304, 306, 
		327, 349, 350, 353, 354, 389, 401, 408, 410, 413, 431, 432, 
		433, 436, 455, 500, 524};
	
	private static final Integer[] SAFE = {25, 51, 69, 73, 76, 92, 
		93, 99, 100, 101, 115, 122, 148, 149, 173, 176, 184, 195, 
		197, 200, 201, 207, 208, 213, 214, 217, 222, 230, 233, 242, 
		245, 250, 251, 253, 258, 267, 269, 270, 276, 284, 303, 309, 
		310, 315, 316, 321, 324, 328, 334, 339, 342, 348, 355, 356, 
		359, 362, 364, 367, 374, 375, 380, 387, 390, 399, 400, 412, 
		425, 429, 430, 450, 454, 474, 482, 499, 503, 505, 525, 528};
	
	private static final Integer[] HATCH = {24, 274, 277, 527};
	
    private static final Integer HUMAN_START = 287;
	
	private static final Integer ALIEN_START = 241;
	
	private static Sector[][] grid = new Sector[(int) (HEIGHT + Math.floor(WIDTH/2))][WIDTH];
	
	private GameMap() {
		allocateMap();
	}
	
	/**
	 * Singleton
	 */
	private static GameMap instance;
	
	public static GameMap getInstance(){
			if(instance == null) 
				instance = new GameMap();
			return instance;
	}
	
	/**
	 * This method allocate the map grid. It's reachable with
	 * map[r+floor(q/2)][q]
	 */
	static void allocateMap() {
		for (int q = 0 ; q < WIDTH; q ++) {
			int qOffset = (int) Math.floor(q/2);
			for (int r = 0; r < HEIGHT; r ++) {
				
				int posNum = WIDTH * (r + qOffset) + q;
				
				if (Arrays.asList(BLOCKED).contains(posNum)) {
					grid[r + qOffset][q] = new SectorBlocked(q, r, -q-r);
					
				} else if (Arrays.asList(SAFE).contains(posNum)) {
					grid[r + qOffset][q] = new SectorSafe(q, r, -q-r);
					
				} else if (Arrays.asList(HATCH).contains(posNum)) {
					grid[r + qOffset][q] = new SectorHatch(q, r, -q-r);
					
				} else if ((int) HUMAN_START == posNum) {
					grid[r + qOffset][q] = new SectorHuman(q, r, -q-r);
					
				} else if ((int) ALIEN_START == posNum) {
					grid[r + qOffset][q] = new SectorAlien(q, r, -q-r);
					
				} else {
					grid[r + qOffset][q] = new SectorDangerous(q, r, -q-r);
				}
			}
		}
	}
	
	public static Sector[][] getGrid() {
		return grid;
	}
	
	/**
	 * @return the sector of humanStart
	 */
	public static Sector humanSectorLocation() {
		return numberToSector(HUMAN_START);
	}
	
	/**
	 * @return the sector of alienStart
	 */
	public static Sector alienSectorLocation() {
		return numberToSector(ALIEN_START);
	}
	
	/**
	 * Convert from number to sector
	 * @param num
	 */
	public static Sector numberToSector(Integer num){
		int q = num % WIDTH;
		int r = (num / WIDTH) - (int) Math.floor(q/2);

		return sectorToGrid(new Sector(q, r, -r-q));
	}
	
	/**
	 * Convert from sector to number
	 * @param pos
	 */
	public static Integer sectorToNum(Sector pos){
		return (Integer) ((23 * (pos.getR() + (int) Math.floor((pos.getQ())/2))) + pos.getQ());
	}
	
	public static Sector sectorToGrid(Sector sec) {
		int qOffset = (int) Math.floor(sec.getQ()/2);
		return grid[sec.getR() + qOffset][sec.getQ()];
	}
	
	/**
	 * @param pos
	 * @return a string with the type of sector pos.
	 */
	public static String typeSector(Sector pos) {
		Integer num = sectorToNum(pos);
		
		if (Arrays.asList(BLOCKED).contains(num)) {
			return "Blocked";
			
		} else if (Arrays.asList(SAFE).contains(num)) {
			return "Safe";
			
		} else if (Arrays.asList(HATCH).contains(num)) {
			return "Hatch";
			
		} else if (num.equals(HUMAN_START)) {
			return "human_start";
			
		} else if (num.equals(ALIEN_START)) {
			return "alien_start";
			
		} else {
			return "Dangerous";
		}
	}
}
