package it.polimi.ingsw.bernardicarfi.server;

import java.io.IOException;
import java.rmi.Remote;
import java.util.List;

/**
 * @author Salvatore Carfì, Anna Bernardi
 * 
 */

/**
 * Remote interface exposed by the clients. 
 * This allows the server to send messages to the clients.
 *
 */
public interface RemoteNotifier extends Remote {
	public void write(String s) throws IOException;
	public String askSector(List<String> moves) throws IOException;
	public void setView() throws IOException;
	public boolean yesNo(String question) throws IOException;
	public int askCard(List<String> cards) throws IOException;
	public boolean useDiscard() throws IOException;
	public void printGrid() throws IOException;
	public void setPlayer(String name, String role) throws IOException;
	public String askAnySector() throws IOException;
}