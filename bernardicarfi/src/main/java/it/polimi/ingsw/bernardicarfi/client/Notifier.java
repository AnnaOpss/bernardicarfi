package it.polimi.ingsw.bernardicarfi.client;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import it.polimi.ingsw.bernardicarfi.common.CLI;
import it.polimi.ingsw.bernardicarfi.common.GUI;
import it.polimi.ingsw.bernardicarfi.common.View;
import it.polimi.ingsw.bernardicarfi.server.RemoteNotifier;

/**
 * @author Carfì Salvatore, Bernardi Anna
 *
 */

/**
 * Method that implements the RemoteNotifier interface.
 */
public class Notifier implements RemoteNotifier {
	private View view;
	private Scanner in = new Scanner(System.in);
	
	/**
	 * Ask the player the view he would like to play with, 1 for CLI, 2 for GUI.
	 */
	@Override
	public void setView() throws IOException {
		String graphic = "";
		do {
			System.out.println("Scegli che interfaccia grafica usare:");
			System.out.println("1 - CLI");
			System.out.println("2 - GUI");
			graphic = in.nextLine();
			if(!"1".equals(graphic)  && !"2".equals(graphic))
				System.out.println("Comando non riconosciuto!");
			if("2".equals(graphic))
				System.out.println("L'interfaccia richiesta non è disponibile!");
		} while (!"1".equals(graphic));
		
		System.out.println("Attendi gli altri giocatori...");

		if("1".equals(graphic))
			view = new CLI();
		else
			view = new GUI();
	}
	
	@Override
	public void write(String s) throws IOException {
		view.write(s);
	}
	
	@Override
	public String askSector(List<String> moves) throws IOException {
		return view.getSector(moves);
	}
	
	@Override
	public String askAnySector() throws IOException {
		return view.getAnySector();
	}
	
	@Override
	public int askCard(List<String> cards) throws IOException {
		return view.askCard(cards);
	}
	
	@Override
	public boolean yesNo(String question) throws IOException {
		return view.yesNo(question);
	}
	
	@Override
	public boolean useDiscard() throws IOException {
		return view.useDiscard();
	}
	
	@Override
	public void printGrid() throws IOException {
		view.printGrid();
	}

	@Override
	public void setPlayer(String name, String role) throws IOException {
		view.setPlayerName(name, role);
	}
}