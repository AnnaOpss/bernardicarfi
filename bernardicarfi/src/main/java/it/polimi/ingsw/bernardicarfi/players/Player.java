package it.polimi.ingsw.bernardicarfi.players;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.bernardicarfi.Match;
import it.polimi.ingsw.bernardicarfi.cards.Card;
import it.polimi.ingsw.bernardicarfi.decks.DeckItem;
import it.polimi.ingsw.bernardicarfi.map.Sector;
import it.polimi.ingsw.bernardicarfi.server.RemoteNotifier;

/**
 * @author Bernardi Anna, Carfi Salvatore
 *
 */

public abstract class Player {
	
	static private final int MAX_ITEMS = 3;
	
	protected List<Card> handOfItems = new ArrayList<Card>(MAX_ITEMS);
	protected int step;
	protected int playerID;
	protected Sector currentPosition;
	protected String role;
	protected boolean ableToDraw;
	protected RemoteNotifier notifier;
	protected String name;
	protected String type;
	
	
	protected abstract void setName(int nameSetter);
	public abstract void playTurn() throws IOException;
	public abstract String getType();
	
	public RemoteNotifier getNotifier() {
		return this.notifier;
	}
	
	public void activateSedatives() {
		ableToDraw = false;
	}
	
	/**
	 * Removes the card c from player's hand of cards.
	 * @param c
	 */
	public void discardCard(int c) {
		DeckItem.refuse(handOfItems.get(c));
		handOfItems.remove(c);
	}
	
	/**
	 * Add the card c to player's hand of cards.
	 * @param newCard
	 * @throws IOException
	 */
	public void addCard(Card newCard) throws IOException {
		handOfItems.add(newCard);
		notifier.write("Hai pescato la carta: " + newCard.name());
	}
	
	/**
	 * The player exit from his current sector.
	 * Set his current position to "newPos" sector.
	 * Enter the "newPos" sector.
	 * @param newPos
	 * @throws IOException
	 */
	public void moveTo(Sector newPos) throws IOException{
		currentPosition.walkOut(this);
		setPosition(newPos);
		newPos.walkIn(this);
	}
	
	/**
	 * Let the player move to reachable sectors.
	 * @return
	 * @throws IOException
	 */
	public Sector move() throws IOException {
		String movement;
		List<String> reachable = new ArrayList<String>();
		Sector choice;
		
		do {
			reachable = currentPosition.reachable(step);
			
			movement = notifier.askSector(reachable);
			
			if (!reachable.contains(movement))
				notifier.write("Settore non disponibile, inserisci un settore tra quelli a scelta.");
			
		} while(!reachable.contains(movement));
		
		choice = Sector.nameToSector(movement); 
		moveTo(choice);
		
		return choice;
	}
	
	public String getName() {
		return name;   
	}
	
	public int getPlayerID() {
		return playerID;
	}
	
	public void increaseStep() {
		step++;
	}
	
	public void decreaseStep() {
		step--;
	}
	
	public void setStep(int step) {
		this.step = step;
	}
	
	public Sector getPosition() {
		return currentPosition;
	}
	
	public void setPosition(Sector position) {
		currentPosition = position;		
	}
	
	/**
	 * Kills a player, adds him to dead players, discards all his cards.
	 * @return
	 * @throws IOException
	 */
	public boolean death() throws IOException {
		int nCards = handOfItems.size();
		
		for (int i = 0; i < nCards ; i++) {
			discardCard(i);
		}
		
		Match.killPlayer(this);
		return true;
	}
	
	public List<Card> getHandOfItems() {
		return handOfItems;
	}
	
	/**
	 * Ask the player to select a card from his hand of cards. Return the index of the card he chose.
	 * @return
	 * @throws IOException
	 */
	public int requestCard() throws IOException {
		List<String> result = new ArrayList<String>();
		
		for (Card card : handOfItems)
			result.add(card.name());
			
		return notifier.askCard(result);
	}
	
	/**
	 * Always (for aliens) return true, overrided from human
	 * @return true
	 */
	public boolean ableToDraw() {
		return true;
	}
}
