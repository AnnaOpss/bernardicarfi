package it.polimi.ingsw.bernardicarfi.decks;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.bernardicarfi.cards.Card;
import it.polimi.ingsw.bernardicarfi.cards.CardNoiseAnywhere;
import it.polimi.ingsw.bernardicarfi.cards.CardNoiseAnywhereOBJ;
import it.polimi.ingsw.bernardicarfi.cards.CardNoiseHere;
import it.polimi.ingsw.bernardicarfi.cards.CardNoiseHereOBJ;
import it.polimi.ingsw.bernardicarfi.cards.CardSilence;

/**
 * @author Bernardi Anna
 *
 */

public class DeckSector extends Deck {
	private static final int NUM_SECTOR_CARDS = 25;
	
	private static List<Card> cards = new ArrayList<Card>(NUM_SECTOR_CARDS);
	
	public DeckSector() {
		DeckSector.setDeck();
	}
	
	/**
	 * This method define a list of all sector cards.
	 */
	public static void setDeck() {
		for (int i = 0; i < 4; i++) {
			cards.add(new CardNoiseHereOBJ());
			cards.add(new CardNoiseAnywhereOBJ());
		}
		for (int i = 0; i < 5; i++) {
			cards.add(new CardSilence());
		}
		for (int i = 0; i < 6; i++) {
			cards.add(new CardNoiseHere());
			cards.add(new CardNoiseAnywhere());
		}
		
		cards = shuffle(cards);
	}
	
	/**
	 * @return a card. If the main deck is empty,
	 * it shuffle the deck of used cards and put them in the main deck.
	 */
	public static Card takeCard() {
		
		if (cards.isEmpty()) {
			setDeck();
		}
		Card result = cards.get(0);
		cards.remove(0);
		return result;
	}
	
	public static int getNumCards() {
		return NUM_SECTOR_CARDS;
	}
	
	public static List<Card> getDeck() {
		return cards;
	}
}
