package it.polimi.ingsw.bernardicarfi.cards;

import java.io.IOException;
import java.rmi.RemoteException;

import it.polimi.ingsw.bernardicarfi.decks.DeckItem;
import it.polimi.ingsw.bernardicarfi.players.Player;

/**
 * @author Carfì Salvatore
 *
 */

public class CardNoiseAnywhereOBJ extends CardNoiseAnywhere{
	
	/**
	 * Activates "CardNoiseAnywhere" to make noise in a sector, also draw an item card.
	 * @throws IOException 
	 * @throws RemoteException 
	 */
	@Override
	public void activateCard(Player p) throws IOException {
		p.getNotifier().write("La carta settore che hai pescato è una carta pesca oggetto."
				+ "\nPeschi una carta");
		p.addCard(DeckItem.takeCard());
		super.activateCard(p);
	}
}
