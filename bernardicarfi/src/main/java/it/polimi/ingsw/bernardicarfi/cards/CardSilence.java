package it.polimi.ingsw.bernardicarfi.cards;

import it.polimi.ingsw.bernardicarfi.Match;
import it.polimi.ingsw.bernardicarfi.players.Player;

import java.io.IOException;

/**
 * @author Carfì Salvatore
 *
 */

public class CardSilence extends Card {
	

/**
 * Makes the player announce "SILENCE".
 */
	@Override
	public void activateCard(Player p) throws IOException {
		Match.notifyAll(p.getName() + ": SILENZIO");
	}
	
	@Override
	public String name() {
		return "Silenzio";
	}	
}
