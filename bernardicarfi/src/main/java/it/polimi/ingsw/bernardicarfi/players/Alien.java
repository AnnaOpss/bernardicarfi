package it.polimi.ingsw.bernardicarfi.players;

import java.io.IOException;

import it.polimi.ingsw.bernardicarfi.Match;
import it.polimi.ingsw.bernardicarfi.cards.Card;
import it.polimi.ingsw.bernardicarfi.map.GameMap;
import it.polimi.ingsw.bernardicarfi.server.RemoteNotifier;

/**
 * @author Salvatore Carfì, Anna Bernardi
 * 
 */

public class Alien extends Player {
	private int turnNumber;
	
	public Alien(int playerID, RemoteNotifier notifier, int nameSetter) {
		step = 2;
		this.playerID = playerID;
		currentPosition = GameMap.alienSectorLocation();
		this.role = "Alien";	
		ableToDraw = true;
		this.notifier = notifier;
		setName(nameSetter);
		type = "alien";
		turnNumber = 1;
	}
	
	@Override
	public String getType() {
		return type;
	}
	
	/**
	 * Let the alien play his turn. First let him  move, then ask him if he wish to attack.
	 */
	@Override
	public void playTurn() throws IOException {
		Match.notifyAll("Turno numero " + turnNumber + ".");
		Match.notifyAll(name + " ha cominciato il suo turno.");
		notifier.printGrid();

		alienMove();
		
		String attackRequest = "Vuoi attaccare nel tuo settore?\n1 - Si\n2 - No";
		
		if (notifier.yesNo(attackRequest))
			attack();
		
		turnNumber ++;
		notifier.write("Turno concluso");
	}
	
	/**
	 * Add a card to the alien's hand of cards, if he has 4 cards already 
	 * (including the one he just draw) he have to discard one.
	 */
	@Override
	public void addCard(Card newCard) throws IOException {
		super.addCard(newCard);
		
		while (handOfItems.size() > 3) {				
			notifier.write("Hai già tre carte. \nScegli la carta da scartare tra quelle che hai in mano o quella appena pescata.");
			
			int card = requestCard();

			discardCard(card);
		}
	}
	
	/**
	 * According to his player ID set the name of the alien
	 */
	protected void setName(int nameID) {
		if (nameID == 0)
			name = "l'ALIENO 1";
		else if (nameID == 1)
			name = "l'ALIENO 2";
		else if (nameID == 2)
			name = "l'ALIENO 3";
		else if (nameID == 3)
			name = "l'ALIENO 4";
	}
	
	/**
	 * Makes an attack in a sector.
	 * @throws IOException
	 */
	public void attack() throws IOException {
		Match.notifyAll(name + " ha effettuato un attacco nel settore " + currentPosition.sectorToName() + ".");
		if(currentPosition.attackFrom(this))
			step = 3;
	}
	
	/**	
	 * 	Let the alien moves, doesn't let him move into and escape hatch sector.
	 * @throws IOException
	 */
	private void alienMove() throws IOException {
		boolean valid = true;
		do {
			if ("Hatch".equals(GameMap.typeSector(move()))) {
				valid = false;
				getNotifier().write("Non puoi entrare in un hatch.");
			}
		} while(!valid);
	}
	
	
}
