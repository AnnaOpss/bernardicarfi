package it.polimi.ingsw.bernardicarfi.cards;

import java.io.IOException;

import it.polimi.ingsw.bernardicarfi.map.Sector;
import it.polimi.ingsw.bernardicarfi.players.Player;

/**
 * @author Carfì Salvatore
 *
 */

public class CardAttack extends Card {
	
	/**
	 * Kills all the players in the current sector, except the player who used it.
	 * @throws IOException 
	 */
	@Override
	public void activateCard(Player p) throws IOException {
		Sector position = p.getPosition();
		position.attackFrom(p);	
	}
	
	@Override
	public String name() {
		return "Attacco";
	}	
}