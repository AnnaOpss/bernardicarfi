package it.polimi.ingsw.bernardicarfi.players;

/**
 * @Author: Salvatore Carfì, Anna Bernardi
 */

import java.io.IOException;
import java.rmi.RemoteException;

import it.polimi.ingsw.bernardicarfi.Match;
import it.polimi.ingsw.bernardicarfi.cards.Card;
import it.polimi.ingsw.bernardicarfi.map.GameMap;
import it.polimi.ingsw.bernardicarfi.server.RemoteNotifier;

public class Human extends Player {
	
	private boolean moved;
	private int turnNumber;
	
	public Human(int playerID, RemoteNotifier notifier, int nameSetter) {
		step = 1;
		this.playerID = playerID;
		currentPosition = GameMap.humanSectorLocation();
		role = "Human";
		ableToDraw = true;
		this.notifier = notifier;
		moved = false;
		setName(nameSetter);
		type = "human";
		turnNumber = 1;
	}
	
	@Override
	public String getType() {
		return type;
	}
	
	/**
	 * Add a card to the hand of cards of the player. If he has already 3 cards let him use or discard one (including the new one).
	 */
	@Override
	public void addCard(Card newCard) throws IOException {
		super.addCard(newCard);
		String tooManyCardsWarning = "Hai già tre carte. \nUsane o scartane una tra quelle che hai in mano o quella appena pescata:\n"
				+ "1 - usa una carta./n2 - scarta una carta.";
		
		while (handOfItems.size() > 3) {				
			boolean decision = notifier.yesNo(tooManyCardsWarning);
			int card = requestCard();
			
			if (decision)
				useCard(card);
			else
				discardCard(card);
		}
	}
	
	/**
	 * Let the Human play his turn, first ask him if he want's to play a card until he says "No" or has no more cards. 
	 * Then ask him to move. Then ask him again to play cards until he says "No" or has no more cards
	 */
	@Override
	public void playTurn() throws IOException {
		boolean answer;
		boolean flag = true;
		moved = false;
		String askCard = null;
		
		notifier.write("Turno numero " + turnNumber + ".");
		Match.notifyAll(name + " ha cominciato il suo turno.");
		notifier.printGrid();

		while (flag) {
			answer = false;
			if (!handOfItems.isEmpty()) {
				if (handOfItems.size() == 1)
					askCard = "Hai " + handOfItems.size() + " carta, vuoi utilizzarle?\n"
							+ "1) Si\n2) No";
					
				else
					askCard = "Hai " + handOfItems.size() + " carte, vuoi utilizzarle?\n"
							+ "1) Si\n2) No";
				answer = notifier.yesNo(askCard);
			}
			
			if (answer) 
				useCard(requestCard());
			else {
				if (!moved){
					move();
					moved = true;
				} else {
					flag = false;
					turnNumber ++;
					endTurn();
	
				}
			}
		}
	}

	/**
	 * If the player has a defense card he have to discard this then he survives, otherwise he dies.
	 * @return true if he is dead (no defense), false if he had a defense card and survived
	 */
	@Override
	public boolean death() throws IOException {
		int i;
		int nCards = handOfItems.size();
		for(i=0; i < nCards; i++) {
			if(handOfItems.get(i).name() == "Defense") {
				Match.notifyAll(name + " usa la carta Defense");
				discardCard(i);
				return false;
			}
		}
		return super.death();
	}
	
	/**
	 * According to his player ID set the name of the human
	 */
	protected void setName(int nameID) {
		if (nameID == 0)
			name = "IL CAPITANO";
		else if (nameID == 1)
			name = "LA PILOTA";
		else if (nameID == 2)
			name = "LO PSICOLOGO";
		else if (nameID == 3)
			name = "IL SOLDATO";
	}
	
	/**
	 * In the end of the turn deny restore the standards settings for the human (if have been changed).
	 */
	public void endTurn() {
		if(!ableToDraw)
			ableToDraw = true;
		if(step != 1)
			step = 1;
		moved = false;	
	}
	
	@Override
	public boolean ableToDraw() {
		return ableToDraw;
	}
	
	/**
	 * Activates a card then discards it from the player's hand.
	 * if it's defense he can't play it. (returns void)
	 * if he already moved and the card is "adrenaline" he can't play it. (returns void)
	 * @throws IOException 
	 * @throws RemoteException 
	 */
	public void useCard(int cardPos) throws IOException {
		if(handOfItems.get(cardPos).name() == "Defense") {
			notifier.write("Non puoi usare una carta difesa al di fuori di un attacco.");
			return;
		}
		if(handOfItems.get(cardPos).name() == "Adrenaline" && moved) {
			notifier.write("Non puoi usare una carta adrenalina dopo aver effettuato un movimento.");
			return;
		}
		Match.notifyAll(name + " usa la carta " + handOfItems.get(cardPos).name());
		handOfItems.get(cardPos).activateCard(this);				
		discardCard(cardPos);
	}

}
