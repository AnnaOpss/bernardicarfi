package it.polimi.ingsw.bernardicarfi.server;

import it.polimi.ingsw.bernardicarfi.Match;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

/**
 * Thread to handle multiple matches.
 * @author Salvatore Carfì, Anna Bernardi
 * 
 */

public class MultipleMatchHandler implements Runnable {
	
	/**
	 * This method get the notifiers of the last open match and calls
	 * setView(), then increments playersNum and in the end launch 
	 * the game. It also handle the winners.
	 */
	@Override
	public void run() {
		Server server = Server.getServer();
		Map<Integer, RemoteNotifier> notifiers = server.getNotifiers();
		System.out.println("RUNNABLE: notifiers size: " + notifiers.size());
		Match match = server.getLastMatch();
		Map<Integer, String> winners= new HashMap<Integer, String>();
		
		for (int i = 0 ; i < notifiers.size(); i++) {
			try {
				RemoteNotifier rn = notifiers.get(i);
				rn.setView();
			} catch (RemoteException e) {
			} catch (IOException e) {
			}
			
			System.out.println("RUNNABLE: Ho settato la view del giocatore " + i);
			
			try {
				match.addPlayer(i, notifiers.get(i));
			} catch (IOException e) {
			}
			
			System.out.println("RUNNABLE:" + server.getLastMatch());
		}
		
		try {
			winners = match.playGame();
		} catch (IOException e) {
		}
		for (Integer i: winners.keySet())
			try {
				notifiers.get(i).write("HAI VINTO!");
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
}
