package it.polimi.ingsw.bernardicarfi.map;

public class SectorSafe extends Sector {
	
	/**
	 * @author Salvatore Carfì, Anna Bernardi
	 * 
	 */
	
	public SectorSafe(int q, int r, int s) {
		super(q, r, s);
	}
}
