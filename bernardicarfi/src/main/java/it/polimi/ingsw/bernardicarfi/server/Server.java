package it.polimi.ingsw.bernardicarfi.server;

import it.polimi.ingsw.bernardicarfi.Match;
import it.polimi.ingsw.bernardicarfi.common.RemoteCallableClient;
import it.polimi.ingsw.bernardicarfi.server.rmi.RMIHandler;
import it.polimi.ingsw.bernardicarfi.server.rmi.RemoteRMIHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Salvatore Carfì, Anna Bernardi
 * 
 */

public class Server {
	
	private static final int MINPLAYERS = 2;
	private static final int MAXPLAYERS = 2;
	
	private static Server server;
	Registry registry = null;
	String rmiName = "RMIHandler";
	String clientName = "Client";
	private static int playersNum;
	private CallableClient client;
	private CallableClient clientStub;
	private RemoteRMIHandler rmiStub;
	private RMIHandler handler;
	private List<Match> matches;
	private int matchIndex;
	private List<Map<Integer, RemoteNotifier>> allNotifiers;
	private boolean started;
	private int indexNotifiers;
	protected Timer timer;
	private boolean timeOut;
	private static final long TIMERPERIOD = 100000;
	
	public Server() {
		playersNum = 0;
		matches = new ArrayList<Match>();
		matchIndex = 0;
		allNotifiers = new ArrayList<Map<Integer, RemoteNotifier>>();
		allNotifiers.add(new HashMap<Integer, RemoteNotifier>());
		started = false;
		indexNotifiers = 0;
		timeOut = false;
	}
	
	/**
	 * Singleton. If server doesn't exist, it instantiates a new one.
	 * @return server
	 */
	public static Server getServer() {
		if (server == null) {
			server = new Server();
		}
		return server;
	}
	
	public static void main(String[] args) throws IOException, NotBoundException{
		getServer().start();
	}
	
	public void start() throws IOException, NotBoundException {
		/**
		*Starting RMI registry
        */
        try {
            //Creating the RMIHandler and its RemoteObject
            handler = new RMIHandler(server);
            rmiStub = (RemoteRMIHandler) UnicastRemoteObject.exportObject(handler, 0);
            
            //Creating the callable client and its RemoteObject
            client = new RemoteCallableClient();
            clientStub = (CallableClient) UnicastRemoteObject.exportObject(client, 4040);
            
            //Creating the registry
            registry = LocateRegistry.createRegistry(2020);
            
            //Binding the remote objects
            registry.bind(rmiName, rmiStub);
            registry.bind(clientName, clientStub);
                       
        } catch (Exception e) {
            System.err.println("RMI exception:");
            registry = null;
            e.printStackTrace();
        }
		
		/**
		 * Starting socket server
		 */
		System.out.println("Starting the server...");
			
		boolean finish = false;
		
		//Loop to query the server to stop the server.
		while(!finish){
			String read = readLine("SERVER: Press Q to exit\n");
			if("Q".equals(read)){
				finish = true;
			}
		}

		/**
		 * Releasing the server resources.
		 */

		if(registry != null)
			registry.unbind(rmiName);

		System.exit(0);
	}
	

	/**
	 * Helper method to read a line from the console when the 
	 * program is started in Eclipse
	 * @param format the format string to be read
	 * @param args arguments for the format
	 * @return the read string
	 * @throws IOException if the program cannot access the stdin
	 */
	private static String readLine(String format, Object... args) throws IOException {
	    if (System.console() != null) {
	        return System.console().readLine(format, args);
	    }
	    System.out.print(String.format(format, args));
	    
	    BufferedReader br = null;
	    InputStreamReader isr = null;
	    String read = null;
	    
	    isr = new InputStreamReader(System.in);
	    br = new BufferedReader(isr);
	    read = br.readLine();
	    
	    return read;
	}
	
	/**
	 * Increments playersNum, if it's the first player to connect it lauch the timer.
	 * @throws IOException
	 */
	public void addPlayer() throws IOException {
		playersNum ++;
		if (playersNum == 1) {
			startTimer();
		}
	}
	
	/**
	 * If there the match is full (has 8 players) 
	 * or the timer finished and there are at least 2 players,
	 * it starts the match.
	 * @throws IOException
	 */
	public void startMatchIfReady() throws IOException {
		System.out.println("SERVER: match going to start?");
		System.out.println("SERVER: timeOut " + timeOut + "playersNum " + playersNum);
		if (playersNum == MAXPLAYERS ||(playersNum == MINPLAYERS && timeOut)) {
			closeTimer();
			timeOut = false;
			System.out.println("SERVER: match started");
			startMatch();
		}
	}
	
	/**
	 * When this method is called it starts a new thread to handle multiple matches
	 * and sets some counters.
	 * @throws IOException
	 */
	public void startMatch() throws IOException {
		matches.add(matchIndex, new Match(playersNum));
		started = true;
		matchIndex ++;
		playersNum = 0;
		//System.out.println("Dimensione di notifiers " + allNotifiers.get(indexNotifiers).size());
		
		MultipleMatchHandler mmh = new MultipleMatchHandler();
		mmh.run();
	}
	
	private void startTimer() {
		timer = new Timer();

		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() { 
				timeOut = true;
				try {
					startMatchIfReady();
				} catch (RemoteException e) {
				} catch (IOException e) {
				}
			}
		}, TIMERPERIOD, TIMERPERIOD);
	}
	
	/**
	 * Close the timer.
	 */
	private void closeTimer() {
		timer.cancel();
	}
	
	public int getPlayersNum() {
		return playersNum;
	}
	
	public Map<Integer, RemoteNotifier> getNotifiers() {
		return allNotifiers.get(allNotifiers.size() - 1);
	}
	
	public Match getLastMatch() {
		return matches.get(matches.size() - 1);
	}
	
	public Match getMatch(Integer matchNum) {
		return matches.get(matchNum);
	}
	
	public int getIndex() {
		return matchIndex;
	}
	
	/**
	 * Add a new notifier to the right match.
	 * @param num
	 * @param notifier
	 */
	public void addNotifier(int num, RemoteNotifier notifier) {
		if (started) {
			indexNotifiers ++;
			started = false;
			allNotifiers.add(indexNotifiers, new HashMap<Integer, RemoteNotifier>());
		}

		allNotifiers.get(indexNotifiers).put((Integer) num, notifier);
	}	
}
