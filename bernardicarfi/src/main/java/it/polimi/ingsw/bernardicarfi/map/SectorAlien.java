package it.polimi.ingsw.bernardicarfi.map;

import java.io.IOException;

import it.polimi.ingsw.bernardicarfi.players.Player;

/**
 * @author Salvatore Carfì, Anna Bernardi
 *
 */

public class SectorAlien extends Sector {

	public SectorAlien(int q, int r, int s) {
		super(q, r, s);
	}
	
	/**
	 * This method should never be called.
	 */
	@Override
	public void walkIn(Player p) throws IOException {
		p.getNotifier().write("Settore non accessibile.");
	}
}
