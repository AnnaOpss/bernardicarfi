package it.polimi.ingsw.bernardicarfi.decks;

import java.util.Collections;
import java.util.List;

import it.polimi.ingsw.bernardicarfi.cards.Card;

/**
 * @author Salvatore Carfì, Anna Bernardi
 * General interface for decks.
 */

public abstract class Deck {
	
	/**
	 * This method shuffle the given deck.
	 */
	public static List<Card> shuffle(List<Card> deck) {
	    Collections.shuffle(deck);
	    return deck;
	}
}
