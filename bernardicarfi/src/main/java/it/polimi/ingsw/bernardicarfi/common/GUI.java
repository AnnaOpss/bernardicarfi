package it.polimi.ingsw.bernardicarfi.common;

import java.io.IOException;
import java.util.List;
import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JSplitPane;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

import java.awt.ComponentOrientation;
import java.awt.Dimension;

import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JScrollPane;


import javax.swing.BoxLayout;
import javax.swing.UIManager;

import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultComboBoxModel;

/**
 * @author Anna Bernardi
 *
 */
public class GUI implements View {
	private static String name = new String();
	private static String role = new String();
	private static String[] movement = new String[]{"L8", "M8"};
	private static String move = new String();
	private boolean flag = false;
	private static boolean start = false;

	private JFrame frmEftaios;

	/**
	 * Launch the application.
	 */
	public static void main() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI(1);
					window.frmEftaios.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public GUI() throws IOException {
		main();
	}
	
	/**
	 * Create the application.
	 * @throws IOException 
	 */
	public GUI(int i) throws IOException {
		initialize();
	}
	
	@Override
	public String endTurn() {
		return "c";
	}

	@Override
	public String getSector(List<String> moves) {
		movement = new String[moves.size()];
		for (int i = 0; i < moves.size(); i++)
			movement[i] = moves.get(i);
		start = true;
		while (!flag) {
		}
		flag = false;
		return move;
	}

	@Override
	public void write(String s) throws IOException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setPlayerName(String n, String r) throws IOException {
		name = n;
		role = r;
		start = true;
	}

	@Override
	public boolean yesNo(String question) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int askCard(List<String> cards) throws IOException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean useDiscard() throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void printGrid() throws IOException {
		// TODO Auto-generated method stub
	}
	
	@Override
	public String getAnySector() throws IOException {
		return null;
	}
	
	@Override
	public String getType() {
		return "GUI";
	}
	
	private void setMove(String s) {
		move = s;
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @throws IOException
	 */
	private void initialize() throws IOException {
		Image map = null;
		map = ImageIO.read(getClass().getResourceAsStream("./map.png"));
		Icon mapIcon = new ImageIcon(map);
		
		Image title = null;
		title = ImageIO.read(getClass().getResourceAsStream("./title.png"));
		Icon titleIcon = new ImageIcon(title);
		
		Image adrenalineCard = null;
		adrenalineCard = ImageIO.read(getClass().getResourceAsStream("./adrenalineCard.png"));
		Icon adrenalineCardIcon = new ImageIcon(adrenalineCard);
/**
		Image attackCard = null;
		attackCard = ImageIO.read(getClass().getResourceAsStream("./images/attackCard.png"));
		Icon attackCardIcon = new ImageIcon(attackCard);
		
		Image defenseCard = null;
		defenseCard = ImageIO.read(getClass().getResourceAsStream("./images/defenseCard.png"));
		Icon defenseCardIcon = new ImageIcon(defenseCard);
		
		Image sedativesCard = null;
		sedativesCard = ImageIO.read(getClass().getResourceAsStream("./images/sedativesCard.png"));
		Icon sedativesCardIcon = new ImageIcon(sedativesCard);
		
		Image spotlightCard = null;
		spotlightCard = ImageIO.read(getClass().getResourceAsStream("./images/spotlightCard.png"));
		Icon spotlightCardIcon = new ImageIcon(spotlightCard);
		
		Image teleportCard = null;
		teleportCard = ImageIO.read(getClass().getResourceAsStream("./images/teleportCard.png"));
		Icon teleportCardIcon = new ImageIcon(teleportCard);
*/
		frmEftaios = new JFrame();
		frmEftaios.setVisible(true);
		frmEftaios.setIconImage(map);
		frmEftaios.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEftaios.getContentPane().setSize(new Dimension(500, 40));
		frmEftaios.getContentPane().setPreferredSize(new Dimension(400, 40));
		frmEftaios.getContentPane().setBackground(new Color(0, 51, 0));
		frmEftaios.getContentPane().setFont(new Font("American Typewriter", Font.PLAIN, 13));
		frmEftaios.getContentPane().setForeground(new Color(0, 102, 0));
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setAlignmentX(Component.RIGHT_ALIGNMENT);
		splitPane.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		splitPane.setDividerSize(1);
		splitPane.setToolTipText("");
		splitPane.setForeground(new Color(0, 102, 0));
		splitPane.setBackground(new Color(0, 51, 0));
		frmEftaios.getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setSize(new Dimension(456, 0));
		panel.setPreferredSize(new Dimension(456, 10));
		panel.setMinimumSize(new Dimension(456, 10));
		panel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		panel.setMaximumSize(new Dimension(456, 32767));
		panel.setBackground(new Color(0, 51, 0));
		panel.setForeground(new Color(0, 153, 0));
		splitPane.setLeftComponent(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JLabel label_2 = new JLabel("");
		label_2.setBorder(new CompoundBorder(new EmptyBorder(10, 10, 10, 10), null));
		label_2.setAlignmentX(Component.CENTER_ALIGNMENT);
		label_2.setIcon(titleIcon);
		panel.add(label_2);
		
		JLabel label = new JLabel(name);
		label.setBorder(new CompoundBorder(new EmptyBorder(10, 0, 10, 0), null));
		label.setAlignmentX(Component.CENTER_ALIGNMENT);
		label.setHorizontalTextPosition(SwingConstants.CENTER);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setMaximumSize(new Dimension(200, 6));
		label.setForeground(new Color(0, 153, 0));
		label.setFont(new Font("American Typewriter", Font.BOLD, 20));
		label.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		label.setBackground(new Color(0, 51, 0));
		panel.add(label);
		
		JLabel lblRuolo = new JLabel(role);
		lblRuolo.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblRuolo.setMaximumSize(new Dimension(200, 16));
		lblRuolo.setHorizontalTextPosition(SwingConstants.CENTER);
		lblRuolo.setBorder(new CompoundBorder(new EmptyBorder(10, 0, 10, 0), null));
		lblRuolo.setHorizontalAlignment(SwingConstants.CENTER);
		lblRuolo.setFont(new Font("American Typewriter", Font.PLAIN, 17));
		lblRuolo.setForeground(new Color(0, 153, 0));
		lblRuolo.setBackground(new Color(0, 51, 0));
		panel.add(lblRuolo);
		
		final JComboBox comboBox = new JComboBox();
		comboBox.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				setMove((String)comboBox.getSelectedItem());
				comboBox.setModel(new DefaultComboBoxModel(new String[] {""}));
				flag = true;
			}
		});
		/**
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setMove((String)comboBox.getSelectedItem());
				comboBox.setModel(new DefaultComboBoxModel(new String[] {""}));
			}
		});
		*/
		comboBox.setForeground(new Color(0, 51, 0));
		comboBox.setBorder(new CompoundBorder(new EmptyBorder(10, 115, 10, 115), null));
		comboBox.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		comboBox.setModel(new DefaultComboBoxModel(movement));
		while (!start){}
		comboBox.setFont(new Font("American Typewriter", Font.PLAIN, 13));
		comboBox.setBackground(UIManager.getColor("ComboBox.background"));
		panel.add(comboBox);
		JButton btnAttacca = new JButton("ATTACCA!");
		btnAttacca.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		btnAttacca.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnAttacca.setPreferredSize(new Dimension(60, 40));
		btnAttacca.setMinimumSize(new Dimension(60, 40));
		btnAttacca.setContentAreaFilled(false);
		btnAttacca.setSize(new Dimension(60, 40));
		btnAttacca.setMaximumSize(new Dimension(200, 40));
		btnAttacca.setHorizontalTextPosition(SwingConstants.CENTER);
		btnAttacca.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		btnAttacca.setForeground(new Color(0, 153, 0));
		btnAttacca.setFont(new Font("American Typewriter", Font.BOLD, 13));
		btnAttacca.setBackground(new Color(0, 51, 0));
		panel.add(btnAttacca);
		
		JPanel panel_1 = new JPanel();
		panel_1.setPreferredSize(new Dimension(10, 120));
		panel_1.setMaximumSize(new Dimension(32767, 600));
		panel_1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		panel_1.setFont(new Font("American Typewriter", Font.PLAIN, 13));
		panel_1.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		panel_1.setForeground(new Color(0, 153, 0));
		panel_1.setBackground(new Color(0, 51, 0));
		panel.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
		
		JButton button_1 = new JButton("");
		button_1.setPreferredSize(new Dimension(50, 29));
		button_1.setMaximumSize(new Dimension(32767, 32767));
		button_1.setHorizontalTextPosition(SwingConstants.CENTER);
		button_1.setForeground(new Color(0, 153, 0));
		button_1.setFont(new Font("American Typewriter", Font.BOLD, 13));
		button_1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		button_1.setBackground(new Color(0, 51, 0));
		button_1.setAlignmentY(1.0f);
		button_1.setActionCommand("USA CARTA");
		button_1.setIcon(adrenalineCardIcon);
		button_1.setBorder(new EmptyBorder(5, 5, 5, 0));
		button_1.setContentAreaFilled(false);
		panel_1.add(button_1);
		
		JButton button = new JButton("");
		button.setPreferredSize(new Dimension(50, 29));
		button.setMaximumSize(new Dimension(32767, 32767));
		button.setHorizontalTextPosition(SwingConstants.CENTER);
		button.setForeground(new Color(0, 153, 0));
		button.setFont(new Font("American Typewriter", Font.BOLD, 13));
		button.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		button.setBackground(new Color(0, 51, 0));
		button.setAlignmentY(1.0f);
		button.setActionCommand("USA CARTA");
		button.setIcon(adrenalineCardIcon);
		button.setBorder(new EmptyBorder(5, 5, 5, 0));
		button.setContentAreaFilled(false);
		panel_1.add(button);
		
		JPanel panel_2 = new JPanel();
		panel_2.setPreferredSize(new Dimension(10, 120));
		panel_2.setMaximumSize(new Dimension(32767, 600));
		panel_2.setForeground(new Color(0, 153, 0));
		panel_2.setFont(new Font("American Typewriter", Font.PLAIN, 13));
		panel_2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		panel_2.setBackground(new Color(0, 51, 0));
		panel_2.setAlignmentY(1.0f);
		panel.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));
		
		JButton button_2 = new JButton("");
		button_2.setPreferredSize(new Dimension(50, 29));
		button_2.setMaximumSize(new Dimension(32767, 32767));
		button_2.setHorizontalTextPosition(SwingConstants.CENTER);
		button_2.setForeground(new Color(0, 153, 0));
		button_2.setFont(new Font("American Typewriter", Font.BOLD, 13));
		button_2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		button_2.setBackground(new Color(0, 51, 0));
		button_2.setAlignmentY(1.0f);
		button_2.setActionCommand("USA CARTA");
		button_2.setIcon(adrenalineCardIcon);
		button_2.setBorder(new EmptyBorder(5, 5, 5, 0));
		button_2.setContentAreaFilled(false);
		panel_2.add(button_2);
		
		JButton button_3 = new JButton("");
		button_3.setPreferredSize(new Dimension(50, 29));
		button_3.setMaximumSize(new Dimension(32767, 32767));
		button_3.setHorizontalTextPosition(SwingConstants.CENTER);
		button_3.setForeground(new Color(0, 153, 0));
		button_3.setFont(new Font("American Typewriter", Font.BOLD, 13));
		button_3.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		button_3.setBackground(new Color(0, 51, 0));
		button_3.setAlignmentY(1.0f);
		button_3.setActionCommand("USA CARTA");
		button_3.setIcon(adrenalineCardIcon);
		button_3.setBorder(new EmptyBorder(5, 5, 5, 0));
		button_3.setContentAreaFilled(false);
		panel_2.add(button_3);
		
		//splitPane destra
		JSplitPane splitPane_1 = new JSplitPane();
		splitPane_1.setSize(new Dimension(810, 750));
		splitPane_1.setPreferredSize(new Dimension(810, 750));
		splitPane_1.setMaximumSize(new Dimension(810, 750));
		splitPane_1.setMinimumSize(new Dimension(810, 750));
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane_1.setDividerSize(1);
		splitPane_1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		splitPane_1.setBackground(new Color(0, 51, 0));
		splitPane.setRightComponent(splitPane_1);
		
		//pannello con label con mappa
		JPanel panel_3 = new JPanel((LayoutManager) null);
		panel_3.setSize(new Dimension(810, 600));
		panel_3.setMaximumSize(new Dimension(810, 600));
		panel_3.setPreferredSize(new Dimension(810, 600));
		panel_3.setMinimumSize(new Dimension(810, 600));
		panel_3.setLayout(new BorderLayout());
		splitPane_1.setLeftComponent(panel_3);

		//label con mappa
		JLabel label_1 = new JLabel("");
		label_1.setSize(new Dimension(810, 600));
		label_1.setPreferredSize(new Dimension(810, 600));
		label_1.setMaximumSize(new Dimension(810, 600));
		label_1.setMinimumSize(new Dimension(810, 600));
		label_1.setIcon(mapIcon);
		panel_3.add(label_1, BorderLayout.CENTER);
		
		//scrolPane basso
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setSize(new Dimension(810, 16));
		scrollPane.setPreferredSize(new Dimension(810, 16));
		scrollPane.setMinimumSize(new Dimension(0, 16));
		scrollPane.setMaximumSize(new Dimension(810, 16));
		scrollPane.setForeground(new Color(0, 153, 0));
		scrollPane.setBackground(new Color(0, 51, 0));
		splitPane_1.setRightComponent(scrollPane);
		frmEftaios.setTitle("EFTAIOS");
		frmEftaios.setFont(new Font("American Typewriter", Font.PLAIN, 12));
		frmEftaios.setBackground(new Color(0, 51, 0));
	}

}
