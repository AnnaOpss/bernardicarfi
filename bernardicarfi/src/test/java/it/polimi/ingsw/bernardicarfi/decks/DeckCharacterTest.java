package it.polimi.ingsw.bernardicarfi.decks;

import it.polimi.ingsw.bernardicarfi.decks.DeckCharacter;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Bernardi Anna
 *
 */

public class DeckCharacterTest {
	
	private int playersNumberOdd = 7;
	private int playersNumberEven = 8;
	
	@Test
    public void testisAlienEven() {
		int alien = 0;
		int human = 0;
		
		DeckCharacter c = new DeckCharacter(playersNumberEven);
		
		int totalAlien = c.getTotalAlien();
		int totalHuman = c.getTotalHuman();
		
		for (int i = 0; i < playersNumberEven; i ++) {
			boolean res = c.isAlien();

			if (res) {
				alien ++;
			} else {
				human ++;
			}	
		}
		
		assertEquals(alien, totalAlien);
		assertEquals(human, totalHuman);
	}
	
	@Test
	public void testisAlienOdd() {
		int alien = 0;
		int human = 0;
		
		DeckCharacter c = new DeckCharacter(playersNumberOdd);
		
		int totalAlien = c.getTotalAlien();
		int totalHuman = c.getTotalHuman();
		
		for (int i = 0; i < playersNumberOdd; i ++) {
			boolean res = c.isAlien();

			if (res) {
				alien ++;
			} else {
				human ++;
			}	
		}
		
		assertEquals(alien, totalAlien);
		assertEquals(human, totalHuman);
	}

}
