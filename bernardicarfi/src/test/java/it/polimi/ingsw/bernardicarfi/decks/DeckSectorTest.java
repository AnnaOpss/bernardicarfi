package it.polimi.ingsw.bernardicarfi.decks;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.bernardicarfi.cards.Card;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class DeckSectorTest {
	
	@Test
	public void testTakeCard() throws IOException {
		DeckSector ds = new DeckSector();
		List<Card> result = new ArrayList<Card>();
		boolean flag = false;
	
		for (int i = 0; i < DeckSector.getNumCards(); i ++) {
			Card card = DeckSector.takeCard();
			result.add(card);
		}
	
		if (result.containsAll(DeckSector.getDeck())) 
			flag = true;
	
		assertTrue(flag);

	}
	
	@Test
	public void testOverTakingCard() throws IOException {
		DeckSector ds = new DeckSector();
		List<Card> result = new ArrayList<Card>();
		boolean flag = false;
	
		for (int i = 0; i < DeckSector.getNumCards(); i ++) {
			Card card = DeckSector.takeCard();
			result.add(card);
		}
	
		if (DeckSector.takeCard() != null)
			flag = true;
	
		assertTrue(flag);

	}
}



