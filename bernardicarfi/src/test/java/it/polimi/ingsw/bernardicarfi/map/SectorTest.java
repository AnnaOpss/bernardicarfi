package it.polimi.ingsw.bernardicarfi.map;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;

/**
 * @author Bernardi Anna
 *
 */

public class SectorTest extends TestCase {
	private final int width = 23;
	
	@Test
    public void testReachableHumanStart1() {
		/**
		 * reachable from Human start pos = 287
		 */
		
		GameMap.allocateMap();
		
		int coloumn = 287 % width;
		int row = (287 / width) - (int) Math.floor(coloumn/2);

		Sector start = new Sector(coloumn, row, -row-coloumn);
		int movement = 1;

		List<String> result = start.reachable(movement);
		
		List<String> correct_result = Arrays.asList("M9", "M8", "L9", "K9", "K8");

		assertEquals(correct_result, result);
	}
	
	@Test
    public void testReachableHumanStart2() {
		/**
		 * reachable from Human start pos = 287
		 */
		
		int coloumn = 287 % width;
		int row = (287 / width) - (int) Math.floor(coloumn/2);

		Sector start = new Sector(coloumn, row, -row-coloumn);
		int movement = 2;
		
		List<String> result = start.reachable(movement);
		
		List<String> correct_result = Arrays.asList("M9", "M8", "L9", "K9", "K8", 
				"N8", "N9", "M10", "L10", "K10", "J8", "J9");
		
		assertEquals(correct_result, result);
	}
	
	@Test
    public void testReachableH123() {
		/**
		 * reachable from Human start pos = 329
		 */
		
		int coloumn = 329 % width;
		int row = (329 / width) - (int) Math.floor(coloumn/2);

		Sector start = new Sector(coloumn, row, -row-coloumn);
		int movement = 3;
		
		List<String> result = start.reachable(movement);
		
		List<String> correct_result = Arrays.asList("I13", "H13", "G13", "G12", 
				"J13", "I14", "H14", "G14", "F11", "G11", "K14", "J14", "E12", 
				"E11", "F10", "G10");
		assertEquals(correct_result, result);
	}
	
	@Test
    public void testReachableCorner() {
		/**
		 * reachable from Human start pos = 23
		 */
		
		int coloumn = 23 % width;
		int row = (23 / width) - (int) Math.floor(coloumn/2);

		Sector start = new Sector(coloumn, row, -row-coloumn);
		int movement = 1;

		List<String> result = start.reachable(movement);
		
		List<String> correct_result = Arrays.asList("B1", "B2", "A3");

		assertEquals(correct_result, result);
	}
	
	@Test
	public void testGetCharForNumber() {
		Sector sec = new Sector(0, 2, -2);
		String c = sec.getCharForNumber(0);
		assertEquals("A", c);
	}
	
	@Test
	public void testSectorToNameL8() {
		Sector sec = new Sector(11, 7, -18);
		String name = sec.sectorToName();
		assertEquals("L8", name);
	}
	
	@Test
	public void testSectorToNameA3() {
		Sector sec = new Sector(0, 2, -2);
		String name = sec.sectorToName();
		assertEquals("A3", name);
	}
	
	@Test
	public void testSectorToNameM7() {
		Sector sec = new Sector(12, 6, -18);
		String name = sec.sectorToName();
		assertEquals("M7", name);
	}
	
	@Test
	public void testSectorToNameM8() {
		Sector sec = new Sector(12, 7, -18);
		String name = sec.sectorToName();
		assertEquals("M8", name);
	}

	@Test
	public void testSectorToNameL7() {
		Sector sec = new Sector(11, 6, -17);
		String name = sec.sectorToName();
		assertEquals("L7", name);
	}
	
	@Test
	public void testSectorToNameE4() {
		Sector sec = new Sector(4, 3, -7);
		String name = sec.sectorToName();
		assertEquals("E4", name);
	}
	
	@Test
	public void testSectorToNameO8() {
		Sector sec = new Sector(15, 7, -22);
		String name = sec.sectorToName();
		assertEquals("P8", name);
	}
	
	@Test
	public void testSectorToNameO14() {
		Sector sec = new Sector(14, 13, -27);
		String name = sec.sectorToName();
		assertEquals("O14", name);
	}
	
	@Test
	public void testSectorToNameW14() {
		Sector sec = new Sector(22, 13, -35);
		String name = sec.sectorToName();
		assertEquals("W14", name);
	}
	
	@Test
	public void testNameToSectorL8() {
		Sector sec = new Sector(15, 7, -22);

		sec = Sector.nameToSector("L8");
		Sector res = new Sector(11, 7, -18);

		assertEquals(true, res.getQ() == sec.getQ() && res.getR() == sec.getR() && res.getS() == sec.getS());
	}
	
	@Test
	public void testNameToSectorW14() {
		Sector sec = new Sector(0, 0, 0);

		sec = Sector.nameToSector("W14");
		Sector res = new Sector(22, 13, -35);
				
		assertEquals(true, res.getQ() == sec.getQ() && res.getR() == sec.getR() && res.getS() == sec.getS());
	}
	
	@Test
	public void testNameToSectorP8() {
		Sector sec = new Sector(0, 0, 0);

		sec = Sector.nameToSector("P8");
		Sector res = new Sector(15, 7, -22);
		
		assertEquals(true, res.getQ() == sec.getQ() && res.getR() == sec.getR() && res.getS() == sec.getS());
	}
	
	@Test
	public void testGetNeighborL8() {
		Sector sector = new Sector(11, 7, -18);
		
		List<Sector> neighbors = sector.getNeighbors();
		List<String> result = new ArrayList<String>();

		for (Sector s : neighbors) {
			String name = s.sectorToName();
			result.add(name);
		}
		
		List<String> correct_result = Arrays.asList("M9", "M8", "L9", "K9", "K8", "L7");
		
		assertEquals(correct_result, result);
	}
	
	@Test
	public void testGetNeighborK9() {
		Sector sector = new Sector(10, 8, -18);
		
		List<Sector> neighbors = sector.getNeighbors();
		List<String> result = new ArrayList<String>();

		for (Sector s : neighbors) {
			String name = s.sectorToName();
			result.add(name);
		}
		
		List<String> correct_result = Arrays.asList("L8", "L9", "K10", "J8", "J9", "K8");
		
		assertEquals(correct_result, result);
	}
	
	@Test
	public void testGetNeighborD5() {
		Sector sector = new Sector(3, 4, -7);
		
		List<Sector> neighbors = sector.getNeighbors();
		List<String> result = new ArrayList<String>();

		for (Sector s : neighbors) {
			String name = s.sectorToName();
			result.add(name);
		}
		
		List<String> correct_result = Arrays.asList("E6", "E5", "D6", "C6", "C5", "D4");
		
		assertEquals(correct_result, result);
	}
	
	@Test
	public void testGetNeighborA3() {
		Sector sector = new Sector(0, 2, -2);

		List<Sector> neighbors = sector.getNeighbors();
		List<String> result = new ArrayList<String>();
		
		for (Sector s : neighbors) {
			if (s != null) {
				String name = s.sectorToName();
				result.add(name);
			}
		}
		
		List<String> correct_result = Arrays.asList("B2", "B3", "A4", "A2");
		
		assertEquals(correct_result, result);
	}
	
	@Test
	public void testIsNeighbor() {
		Sector sec = new Sector(11, 7, -18);
		Sector neighbor = new Sector(12, 7, -19);
		assertEquals(true, sec.isNeighbor(neighbor));
	}
}
