package it.polimi.ingsw.bernardicarfi.map;

import static org.junit.Assert.*;

import org.junit.Test;

public class MapTest {
	
	@Test
	public void testAllocateMap() {
		//Map.allocateMap();
		
		Sector[][] map = GameMap.getGrid();
		
		for (int i = 0; i < 14; i++) {
			System.out.println(GameMap.sectorToNum(map[i][0]) + " " + GameMap.typeSector(map[i][0]) + " " + GameMap.sectorToNum(map[i][1]) + " " + GameMap.typeSector(map[i][1]));
		}
		
		System.out.println();
		
		for (int i = 1; i < 14 + 1; i++) {
			System.out.println(GameMap.sectorToNum(map[i][2]) + " " + GameMap.typeSector(map[i][2]) + " " + GameMap.sectorToNum(map[i][3]) + " " + GameMap.typeSector(map[i][3]));
		}
	}
	
	@Test
	public void testSectorToNum287() {
		Sector sec = new Sector(11, 7, -18);
		Integer num = GameMap.sectorToNum(sec);
		int res = num.intValue();
		assertEquals(287, res);
	}
	
	@Test
	public void testSectorToNum48() {
		Sector sec = new Sector(2, 1, -3);
		Integer num = GameMap.sectorToNum(sec);
		int res = num.intValue();
		assertEquals(48, res);
	}
	
	@Test
	public void testSectorToNum25() {
		Sector sec = new Sector(2, 0, -2);
		Integer num = GameMap.sectorToNum(sec);
		int res = num.intValue();
		assertEquals(25, res);
	}
	
	@Test
	public void testSectorToNum23() {
		GameMap.allocateMap();
		Sector sec = new Sector(0, 1, -1);
		Integer num = GameMap.sectorToNum(sec);
		int res = num.intValue();
		assertEquals(23, res);
	}
	
	@Test
	public void testSectorToNum0() {
		Sector sec = new Sector(0, 0, 0);
		Integer num = GameMap.sectorToNum(sec);
		int res = num.intValue();
		assertEquals(0, res);
	}
	
	@Test
	public void testNumberToSector287() {
		Sector sec = new Sector(11, 7, -18);
		Integer num = 287;
		Sector res = GameMap.numberToSector(num);
		assertEquals(true, res.getQ() == sec.getQ() && res.getR() == sec.getR() && res.getS() == sec.getS());
	}
	
	@Test
	public void testNumberToSector337() {
		Sector sec = new Sector(15, 7, -22);
		Integer num = 337;
		Sector res = GameMap.numberToSector(num);
		assertEquals(true, res.getQ() == sec.getQ() && res.getR() == sec.getR() && res.getS() == sec.getS());
	}
	
	@Test
	public void testNumberToSector118() {
		Sector sec = new Sector(3, 4, -7);
		Integer num = 118;
		
		Sector res = GameMap.numberToSector(num);
		assertEquals(true, res.getQ() == sec.getQ() && res.getR() == sec.getR() && res.getS() == sec.getS());
	}
	
	@Test
	public void testNumberToSector119() {
		Sector sec = new Sector(4, 3, -7);
		Integer num = 119;
		
		Sector res = GameMap.numberToSector(num);
		assertEquals(true, res.getQ() == sec.getQ() && res.getR() == sec.getR() && res.getS() == sec.getS());
	}
	
	
	@Test
	public void testTypeSectorHumanStart() {
		Sector sec = new Sector(11, 7, -18);
		String res = GameMap.typeSector(sec);
		assertEquals("human_start", res);
	}
	
	@Test
	public void testTypeSectorDangerous() {
		Sector sec = new Sector(15, 7, -22);
		String res = GameMap.typeSector(sec);
		assertEquals("Dangerous", res);
	}
	
	@Test
	public void testSectorToGrid() {
		Sector sec = new Sector(11, 7, -18);
		String value = GameMap.typeSector(sec);
		
		Sector test = GameMap.sectorToGrid(sec);
		String result = GameMap.typeSector(test);
		
		assertEquals(value, result);
	}
	
	@Test
	public void testHumanSectorLocation() {
		Sector res = GameMap.humanSectorLocation();
		String result = GameMap.typeSector(res);
		
		Sector sec = new Sector(11, 7, -18);
		String value = GameMap.typeSector(sec);
		
		assertEquals(value, result);
	}
	
	@Test
	public void testAlienSectorLocation() {
		Sector res = GameMap.alienSectorLocation();
		String result = GameMap.typeSector(res);
		
		Sector sec = new Sector(11, 5, -16);
		String value = GameMap.typeSector(sec);
		
		assertEquals(value, result);
	}
}
