package it.polimi.ingsw.bernardicarfi.cards;

import static org.junit.Assert.*;

import org.junit.Test;

public class CardLightTest {
	
	@Test
	public void testName() {
		CardLight card = new CardLight();
		assertEquals("Spotlight", card.name());
	}
}

