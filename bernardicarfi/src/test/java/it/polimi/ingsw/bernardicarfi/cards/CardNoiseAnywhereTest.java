package it.polimi.ingsw.bernardicarfi.cards;

import static org.junit.Assert.*;

import org.junit.Test;

public class CardNoiseAnywhereTest {
	
	@Test
	public void testName() {
		CardNoiseAnywhere card = new CardNoiseAnywhere();
		assertEquals("Rumore in un altro settore", card.name());
	}
}
