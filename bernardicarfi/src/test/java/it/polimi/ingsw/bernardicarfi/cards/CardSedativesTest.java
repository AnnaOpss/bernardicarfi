package it.polimi.ingsw.bernardicarfi.cards;

import static org.junit.Assert.*;

import org.junit.Test;

public class CardSedativesTest {
	
	@Test
	public void testName() {
		CardSedatives card = new CardSedatives();
		assertEquals("Sedativo", card.name());
	}
}
