package it.polimi.ingsw.bernardicarfi.decks;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.bernardicarfi.cards.Card;

import org.junit.Test;

public class DeckItemTest {

	@Test
	public void testTakeCard() throws IOException {
		DeckItem di = new DeckItem();
		List<Card> result = new ArrayList<Card>();
		boolean flag = false;
		
		for (int i = 0; i < DeckItem.getNumCards(); i ++) {
			Card card = DeckItem.takeCard();
			result.add(card);
		}
		
		if (result.containsAll(DeckItem.getDeck()))
			flag = true;
		
		assertTrue(flag);
	}
	
	@Test
	public void testRefuse() throws IOException {
		DeckItem di = new DeckItem();
		Card card = DeckItem.takeCard();
		DeckItem.refuse(card);
		
		assertEquals(1, DeckItem.getRefuses().size());
	}
}
