package it.polimi.ingsw.bernardicarfi.cards;

import static org.junit.Assert.*;

import org.junit.Test;

public class CardTeleportTest {
	
	@Test
	public void testName() {
		CardTeleport card = new CardTeleport();
		assertEquals("Teletrasporto", card.name());
	}
}
