package it.polimi.ingsw.bernardicarfi.decks;

import it.polimi.ingsw.bernardicarfi.decks.DeckEscapeHatch;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Bernardi Anna
 *
 */

public class DeckEscapeHatchTest {
		
	@Test
    public void testisDamaged() {
		int broken = 0;
		int unbroken = 0;
		
		DeckEscapeHatch es = new DeckEscapeHatch();
		
		assertEquals(4, DeckEscapeHatch.leftHatches());
		
		for (int i = 0; i < 4; i ++) {
			boolean res4 = DeckEscapeHatch.isDamaged();
			if (res4) {
				broken ++;
			} else {
				unbroken ++;
			}
		}
		assertTrue(broken <= 3);
		assertTrue(unbroken <= 3);
		assertEquals(4, broken + unbroken);
		assertEquals(0, DeckEscapeHatch.leftHatches());
	}
}
