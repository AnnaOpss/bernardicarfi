package it.polimi.ingsw.bernardicarfi.cards;

import static org.junit.Assert.*;

import org.junit.Test;

public class CardSilenceTest {
	
	@Test
	public void testName() {
		CardSilence card = new CardSilence();
		assertEquals("Silenzio", card.name());
	}
}

