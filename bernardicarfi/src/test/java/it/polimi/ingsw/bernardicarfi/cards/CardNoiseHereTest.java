package it.polimi.ingsw.bernardicarfi.cards;

import static org.junit.Assert.*;

import org.junit.Test;

public class CardNoiseHereTest {
	
	@Test
	public void testName() {
		CardNoiseHere card = new CardNoiseHere();
		assertEquals("Rumore nel tuo settore", card.name());
	}
}

