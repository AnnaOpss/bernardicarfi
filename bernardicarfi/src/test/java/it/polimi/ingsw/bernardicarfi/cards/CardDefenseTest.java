package it.polimi.ingsw.bernardicarfi.cards;

import static org.junit.Assert.*;

import org.junit.Test;

public class CardDefenseTest {
	
	@Test
	public void testName() {
		CardDefense card = new CardDefense();
		assertEquals("Difesa", card.name());
	}
}

