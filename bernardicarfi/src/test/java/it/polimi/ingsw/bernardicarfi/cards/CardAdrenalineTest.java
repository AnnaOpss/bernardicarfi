package it.polimi.ingsw.bernardicarfi.cards;

import static org.junit.Assert.*;

import org.junit.Test;

public class CardAdrenalineTest {
	
	@Test
	public void testName() {
		CardAdrenaline card = new CardAdrenaline();
		assertEquals("Adrenalina", card.name());
	}
}
